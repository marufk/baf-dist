(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["response-request-response-request-module"],{

/***/ "./src/app/layout/response-request/response-request-routing.module.ts":
/*!****************************************************************************!*\
  !*** ./src/app/layout/response-request/response-request-routing.module.ts ***!
  \****************************************************************************/
/*! exports provided: ResponseRequestRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ResponseRequestRoutingModule", function() { return ResponseRequestRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _response_request_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./response-request.component */ "./src/app/layout/response-request/response-request.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var routes = [
    {
        path: '',
        component: _response_request_component__WEBPACK_IMPORTED_MODULE_2__["ResponseRequestComponent"]
    }
];
var ResponseRequestRoutingModule = /** @class */ (function () {
    function ResponseRequestRoutingModule() {
    }
    ResponseRequestRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], ResponseRequestRoutingModule);
    return ResponseRequestRoutingModule;
}());



/***/ }),

/***/ "./src/app/layout/response-request/response-request.component.html":
/*!*************************************************************************!*\
  !*** ./src/app/layout/response-request/response-request.component.html ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\n    <div class=\"form-group col-md-4 filterdiv\">\n        <input class=\"form-control\" [(ngModel)]=\"searchText\" placeholder=\"Filter By Email\">\n    </div>\n</div>\n<div [@routerTransition]>\n    <!-- <app-page-header [heading]=\"'Response Request'\" [icon]=\"'fas fa-undo-alt'\"></app-page-header> -->\n    <ngb-accordion #acc=\"ngbAccordion\" activeIds=\"\" *ngFor=\"let user of users | emailfilter:searchText | paginate: config\">\n        <ngb-panel title=\"Email: {{user[1]}}\">\n            <ng-template ngbPanelContent>\n                <div>\n                    <span><strong>Name: </strong> <span>{{user[0]}}</span></span>\n                </div>\n                <hr>\n                <div style=\"text-align:center\">\n                    <button type=\"button\" class=\"btn btn-outline-success\" (click)=\"Accept(user[1])\">Accept</button>&nbsp;&nbsp;\n                    <button type=\"button\" class=\"btn btn-outline-danger\" (click)=\"Reject(user[1])\">Reject</button>\n                </div>\n            </ng-template>\n        </ngb-panel>\n    </ngb-accordion>\n    <pagination-controls *ngIf=\"users\" class=\"my-pagination\" (pageChange)=\"pageChange($event)\"></pagination-controls>\n</div>\n"

/***/ }),

/***/ "./src/app/layout/response-request/response-request.component.scss":
/*!*************************************************************************!*\
  !*** ./src/app/layout/response-request/response-request.component.scss ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "::ng-deep div.card-header:hover {\n  background-color: #575757; }\n\n::ng-deep div.card-header .btn-link {\n  color: white;\n  text-decoration: none !important; }\n\n::ng-deep div.card-header {\n  border: 2px solid white;\n  background-color: #000000de; }\n\n::ng-deep div.card-body {\n  background-color: #000000de;\n  color: white; }\n\n.filterdiv {\n  margin-right: -15px;\n  float: right; }\n\n.filterdiv input {\n    border: 0;\n    border-bottom: 2px solid #e41a06;\n    background-color: #212121;\n    color: white; }\n\n.my-pagination /deep/ .ngx-pagination {\n  margin-top: 20px;\n  background: #575757 !important; }\n"

/***/ }),

/***/ "./src/app/layout/response-request/response-request.component.ts":
/*!***********************************************************************!*\
  !*** ./src/app/layout/response-request/response-request.component.ts ***!
  \***********************************************************************/
/*! exports provided: ResponseRequestComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ResponseRequestComponent", function() { return ResponseRequestComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _router_animations__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../router.animations */ "./src/app/router.animations.ts");
/* harmony import */ var _shared_services_bank_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../shared/services/bank.service */ "./src/app/shared/services/bank.service.ts");
/* harmony import */ var angular2_flash_messages__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! angular2-flash-messages */ "./node_modules/angular2-flash-messages/module/index.js");
/* harmony import */ var angular2_flash_messages__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(angular2_flash_messages__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var ResponseRequestComponent = /** @class */ (function () {
    // usersres = [
    //  {email: "maruf@gmail.com" , name: "Maruf Khan"},
    //  {email: "adnan@gmail.com" , name: "Adnan"},
    //  {email: "usama@gmail.com" , name: "Usama"}, 
    //  {email: "usman@gmail.com" , name: "Usman"}, 
    //  {email: "khurram@gmail.com" , name: "Khurram Lodhi"}  
    // ]
    function ResponseRequestComponent(service, router, route, flashMessages) {
        var _this = this;
        this.service = service;
        this.router = router;
        this.route = route;
        this.flashMessages = flashMessages;
        this.config = {
            currentPage: 1,
            itemsPerPage: 10
        };
        this.route.queryParamMap
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["map"])(function (params) { return params.get('page'); }))
            .subscribe(function (page) { return _this.config.currentPage = page; });
    }
    ResponseRequestComponent.prototype.pageChange = function (newPage) {
        this.router.navigate(['user-pending-request/'], { queryParams: { page: newPage } });
    };
    ResponseRequestComponent.prototype.ngOnInit = function () {
        this.GetUser();
    };
    ResponseRequestComponent.prototype.Accept = function (useremail) {
        var _this = this;
        var res = {
            enable: true,
            email: useremail,
        };
        if (confirm("Are you sure you want to Accept?")) {
            // const token = this.service.getToken();
            this.service.ApprovedUser(res).subscribe(function (data) {
                _this.flashMessages.show('User Request Accepted!', { cssClass: 'alert-success', timeout: 3000 });
                console.log(data);
            });
            this.router.navigateByUrl('users');
            setTimeout(function () {
                _this.router.navigateByUrl('user-pending-request');
            }, 100);
        }
    };
    ResponseRequestComponent.prototype.Reject = function (useremail) {
        var _this = this;
        var res = {
            enable: false,
            email: useremail,
        };
        if (confirm("Are you sure you want to Reject?")) {
            // const token = this.service.getToken();
            this.service.DeleteUser(res).subscribe(function (data) {
                _this.flashMessages.show('User Request Rejected!', { cssClass: 'alert-danger', timeout: 3000 });
                console.log(data);
            });
            this.router.navigateByUrl('users');
            setTimeout(function () {
                _this.router.navigateByUrl('user-pending-request');
            }, 100);
        }
    };
    ResponseRequestComponent.prototype.GetUser = function () {
        var _this = this;
        // const token = this.service.getToken();
        this.service.getPendingUser().subscribe(function (data) {
            _this.users = data;
            console.log(_this.users);
        }, function (err) {
            if (err.status == 0 || err.status == 401) {
                _this.flashMessages.show('Session Expired Please Login again', { cssClass: 'alert-danger', timeout: 3000 });
                _this.service.RemoveToken();
                console.log('Session Expired');
            }
        });
    };
    ResponseRequestComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-response-request',
            template: __webpack_require__(/*! ./response-request.component.html */ "./src/app/layout/response-request/response-request.component.html"),
            styles: [__webpack_require__(/*! ./response-request.component.scss */ "./src/app/layout/response-request/response-request.component.scss")],
            animations: [Object(_router_animations__WEBPACK_IMPORTED_MODULE_1__["routerTransition"])()]
        }),
        __metadata("design:paramtypes", [_shared_services_bank_service__WEBPACK_IMPORTED_MODULE_2__["BankService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"],
            angular2_flash_messages__WEBPACK_IMPORTED_MODULE_3__["FlashMessagesService"]])
    ], ResponseRequestComponent);
    return ResponseRequestComponent;
}());



/***/ }),

/***/ "./src/app/layout/response-request/response-request.module.ts":
/*!********************************************************************!*\
  !*** ./src/app/layout/response-request/response-request.module.ts ***!
  \********************************************************************/
/*! exports provided: ResponseRequestModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ResponseRequestModule", function() { return ResponseRequestModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/index.js");
/* harmony import */ var _response_request_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./response-request-routing.module */ "./src/app/layout/response-request/response-request-routing.module.ts");
/* harmony import */ var _response_request_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./response-request.component */ "./src/app/layout/response-request/response-request.component.ts");
/* harmony import */ var _shared__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./../../shared */ "./src/app/shared/index.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _my_pipe_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../my-pipe.module */ "./src/app/layout/my-pipe.module.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







// import { EmailfilterPipe } from '../../shared/pipes/emailfilter.pipe';

// import { NgxPaginationModule } from 'ngx-pagination';
var ResponseRequestModule = /** @class */ (function () {
    function ResponseRequestModule() {
    }
    ResponseRequestModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _response_request_routing_module__WEBPACK_IMPORTED_MODULE_3__["ResponseRequestRoutingModule"],
                _shared__WEBPACK_IMPORTED_MODULE_5__["PageHeaderModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormsModule"],
                // NgxPaginationModule,
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_2__["NgbModule"].forRoot(),
                _my_pipe_module__WEBPACK_IMPORTED_MODULE_7__["MyPipeModule"]
            ],
            declarations: [_response_request_component__WEBPACK_IMPORTED_MODULE_4__["ResponseRequestComponent"]]
        })
    ], ResponseRequestModule);
    return ResponseRequestModule;
}());



/***/ })

}]);
//# sourceMappingURL=response-request-response-request-module.js.map