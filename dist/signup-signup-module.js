(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["signup-signup-module"],{

/***/ "./src/app/signup/signup-routing.module.ts":
/*!*************************************************!*\
  !*** ./src/app/signup/signup-routing.module.ts ***!
  \*************************************************/
/*! exports provided: SignupRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SignupRoutingModule", function() { return SignupRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _signup_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./signup.component */ "./src/app/signup/signup.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var routes = [
    {
        path: '', component: _signup_component__WEBPACK_IMPORTED_MODULE_2__["SignupComponent"]
    }
];
var SignupRoutingModule = /** @class */ (function () {
    function SignupRoutingModule() {
    }
    SignupRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], SignupRoutingModule);
    return SignupRoutingModule;
}());



/***/ }),

/***/ "./src/app/signup/signup.component.html":
/*!**********************************************!*\
  !*** ./src/app/signup/signup.component.html ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"login-page\" [@routerTransition]>\n    <div class=\"row justify-content-md-center\">\n        <div class=\"col-md-4 crytal-black\">\n            <img class=\"user-avatar\" src=\"assets/images/BA-logo.png\" width=\"70px\" />\n            <h1 class=\"card-title\">Register Here</h1>\n            <form method=\"post\" role=\"form\" [formGroup]=\"form\" (submit)=\"onRegisterSubmit()\">\n                <div class=\"form-content\">\n                    <div class=\"form-group\">\n                        <div [ngClass]=\"{'has-error': (form.controls.username.errors && form.controls.username.dirty), 'has-success': !form.controls.username.errors && usernameValid}\">\n                            <input type=\"text\" class=\"form-control input-underline input-lg\" placeholder=\"Full Name\"\n                                name=\"username\" formControlName=\"username\" pattern=\"[a-zA-Z][a-zA-Z ]+\">\n                            <ul class=\"help-block text-white\">\n                                <li *ngIf=\"form.controls.username.errors?.required && form.controls.username.dirty && form.controls.username.touched\">This\n                                    field is required</li>\n                                <li *ngIf=\"form.controls.username.errors?.minlength && form.controls.username.dirty || form.controls.username.errors?.maxlength && form.controls.username.dirty \">Minimum\n                                    characters: 3, Maximum characters: 20</li>\n                                <li *ngIf=\"form.controls.username.errors?.validateUsername && form.controls.username.dirty\">Username\n                                    must not have any special characters</li>\n                            </ul>\n                        </div>\n                    </div>\n\n                    <div class=\"form-group\">\n                        <div [ngClass]=\"{'has-error': (form.controls.email.errors && form.controls.email.dirty) || (!emailValid && form.controls.email.dirty), 'has-success': !form.controls.email.errors && emailValid}\">\n                            <input type=\"text\" class=\"form-control input-underline input-lg\" placeholder=\"Email\" name=\"email\"\n                                formControlName=\"email\" email>\n                            <ul class=\"help-block text-white\">\n                                <li *ngIf=\"form.controls.email.errors?.required && form.controls.email.dirty\">This\n                                    field is required</li>\n                                <li *ngIf=\"(form.controls.email.errors?.minlength && form.controls.email.dirty && form.controls.email.dirty ) && form.controls.email.dirty\">Minimum\n                                    characters: 3</li>\n                                <li *ngIf=\"!form.controls.email.valid && form.controls.email.dirty\">This must be a\n                                    valid e-mail</li>\n                                <li *ngIf=\"emailMessage\">{{ emailMessage}}</li>\n                            </ul>\n                        </div>\n                    </div>\n\n                    <div class=\"form-group\">\n                        <div [ngClass]=\"{'has-error': (form.controls.password.errors && form.controls.password.dirty), 'has-success': !form.controls.password.errors}\">\n                            <input type=\"password\" class=\"form-control input-underline input-lg\" placeholder=\"Password\"\n                                name=\"password\" formControlName=\"password\">\n                            <ul class=\"help-block text-white\">\n                                <li *ngIf=\"form.controls.password.errors?.required && form.controls.password.dirty\">This\n                                    field is required</li>\n                                <li *ngIf=\"form.controls.password.errors?.minlength && form.controls.password.dirty || form.controls.password.errors?.maxlength && form.controls.password.dirty \">Minimum\n                                    characters: 6, Maximum characters: 16</li>\n                                <li *ngIf=\"form.controls.password.errors?.validatePassword && form.controls.password.dirty\">Password\n                                    must be at least 6 characters but not more than 16</li>\n                            </ul>\n                        </div>\n                    </div>\n                    \n                    <div class=\"form-group\">\n                        <div [ngClass]=\"{'has-error': (form.controls.confirmpass.errors && form.controls.confirmpass.dirty) || (form.errors?.matchingPasswords && form.controls.confirmpass.dirty), 'has-success': !form.controls.confirmpass.errors && !form.errors?.matchingPasswords}\">\n                            <input type=\"password\" class=\"form-control input-underline input-lg\" placeholder=\"Confirm Password\"\n                                name=\"confirmpass\" formControlName=\"confirmpass\">\n                            <ul class=\"help-block text-white\">\n                                <li *ngIf=\"form.controls.confirmpass.errors?.required && form.controls.confirmpass.dirty\">This\n                                    field is required</li>\n                                <li *ngIf=\"form.errors?.matchingPasswords && form.controls.confirmpass.dirty\">Password\n                                    do not match</li>\n                            </ul>\n                        </div>\n                    </div>\n                </div>\n                <button class=\"btn rounded-btn\" type=\"reset\" > Clear </button>&nbsp;&nbsp;\n                <button class=\"btn rounded-btn\" type=\"submit\" [disabled]=\"!form.valid\"> Register </button>&nbsp;\n                <div class=\"bottom\">\n                    <p>Are you already Registered ? </p>\n                    <a [routerLink]=\"['/login']\">Login Now</a>\n                </div>\n                <!-- <a class=\"btn rounded-btn\" [routerLink]=\"['/login']\"> Log in </a> -->\n            </form>\n        </div>\n    </div>\n</div>\n"

/***/ }),

/***/ "./src/app/signup/signup.component.scss":
/*!**********************************************!*\
  !*** ./src/app/signup/signup.component.scss ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ":host {\n  display: block; }\n\n.login-page {\n  position: absolute;\n  top: 0;\n  left: 0;\n  right: 0;\n  bottom: 0;\n  overflow: auto;\n  text-align: center;\n  color: #fff;\n  padding: 3em; }\n\n.login-page .crytal-black {\n    background: rgba(0, 0, 0, 0.658824);\n    padding: 25px; }\n\n.login-page .crytal-black .bottom {\n      padding-top: 8px;\n      font-size: 14px; }\n\n.login-page .crytal-black .bottom p {\n        margin-bottom: 0rem; }\n\n.login-page .crytal-black .bottom a {\n        color: #ced4da;\n        text-decoration: none; }\n\n.login-page .crytal-black .bottom a:hover {\n          color: #bc2418; }\n\n.login-page .crytal-black .forgot-pass {\n      float: right;\n      margin-top: -55px;\n      font-size: 13px; }\n\n.login-page .crytal-black .forgot-pass a {\n        color: #ced4da;\n        text-decoration: none; }\n\n.login-page .crytal-black .forgot-pass a:hover {\n          color: #bc2418; }\n\n.login-page .col-lg-4 {\n    padding: 0; }\n\n.login-page .input-lg {\n    height: 46px;\n    padding: 10px 16px;\n    font-size: 18px;\n    line-height: 1.3333333;\n    border-radius: 0; }\n\n.login-page .input-underline {\n    background: 0 0;\n    border: none;\n    box-shadow: none;\n    border-bottom: 2px solid rgba(255, 255, 255, 0.5);\n    color: #fff;\n    border-radius: 0; }\n\n.login-page .input-underline:focus {\n    border-bottom: 2px solid #fff;\n    box-shadow: none; }\n\n.login-page .rounded-btn {\n    border-radius: 50px;\n    color: rgba(255, 255, 255, 0.8);\n    background: #222;\n    border: 2px solid rgba(255, 255, 255, 0.8);\n    font-size: 18px;\n    line-height: 40px;\n    padding: 0 25px; }\n\n.login-page .rounded-btn:hover,\n  .login-page .rounded-btn:focus,\n  .login-page .rounded-btn:active,\n  .login-page .rounded-btn:visited {\n    color: white;\n    border: 2px solid white;\n    outline: none; }\n\n.login-page h1 {\n    font-weight: 300;\n    margin-top: 20px;\n    margin-bottom: 10px;\n    font-size: 36px; }\n\n.login-page h1 small {\n      color: rgba(255, 255, 255, 0.7); }\n\n.login-page .form-group {\n    padding: 8px 0; }\n\n.login-page .form-group input::-webkit-input-placeholder {\n      color: rgba(255, 255, 255, 0.6) !important; }\n\n.login-page .form-group input:-moz-placeholder {\n      /* Firefox 18- */\n      color: rgba(255, 255, 255, 0.6) !important; }\n\n.login-page .form-group input::-moz-placeholder {\n      /* Firefox 19+ */\n      color: rgba(255, 255, 255, 0.6) !important; }\n\n.login-page .form-group input:-ms-input-placeholder {\n      color: rgba(255, 255, 255, 0.6) !important; }\n\n.login-page .form-content {\n    padding: 40px 0; }\n\n.login-page .user-avatar {\n    -webkit-border-radius: 50%; }\n\n@media (min-width: 768px) and (max-width: 1200px) {\n    .login-page .col-md-4 {\n      flex: 0 0 42.333333%;\n      max-width: 42.333333%; } }\n\n.input-underline.ng-invalid.ng-touched, select.ng-invalid.ng-touched {\n  border-bottom: 2px solid red; }\n\n.help-block {\n  font-size: 14px;\n  float: left;\n  text-align: left; }\n"

/***/ }),

/***/ "./src/app/signup/signup.component.ts":
/*!********************************************!*\
  !*** ./src/app/signup/signup.component.ts ***!
  \********************************************/
/*! exports provided: SignupComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SignupComponent", function() { return SignupComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _router_animations__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../router.animations */ "./src/app/router.animations.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _shared_services_bank_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../shared/services/bank.service */ "./src/app/shared/services/bank.service.ts");
/* harmony import */ var angular2_flash_messages__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! angular2-flash-messages */ "./node_modules/angular2-flash-messages/module/index.js");
/* harmony import */ var angular2_flash_messages__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(angular2_flash_messages__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var SignupComponent = /** @class */ (function () {
    function SignupComponent(formBuilder, flashMessages, service, router) {
        this.formBuilder = formBuilder;
        this.flashMessages = flashMessages;
        this.service = service;
        this.router = router;
        this.createForm();
    }
    SignupComponent.prototype.ngOnInit = function () {
        // this.service.postApi("adsaaf").subscribe(data=>{
        //     console.log(data);
        // })
    };
    SignupComponent.prototype.createForm = function () {
        this.form = this.formBuilder.group({
            // firstname Input
            username: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required,
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].minLength(3),
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].maxLength(20),
                    this.validateUsername // Custom validation
                ])],
            // Email Input
            email: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required,
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].minLength(3),
                ])],
            // Password Input
            password: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required,
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].minLength(6),
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].maxLength(16),
                ])],
            // Confirm Password Input
            confirmpass: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required] // Field is required
        }, {
            validator: this.matchingPasswords('password', 'confirmpass')
        });
    };
    // Funciton to ensure passwords match
    SignupComponent.prototype.matchingPasswords = function (password, confirmpass) {
        return function (group) {
            // Check if both fields are the same
            if (group.controls[password].value === group.controls[confirmpass].value) {
                return null; // Return as a match
            }
            else {
                return { 'matchingPasswords': true }; // Return as error: do not match
            }
        };
    };
    // Function to validate username is proper format
    SignupComponent.prototype.validateUsername = function (controls) {
        // Create a regular expression
        var regExp = new RegExp(/^[a-zA-Z][a-zA-Z ]+$/);
        // Test username against regular expression
        if (regExp.test(controls.value)) {
            return null; // Return as valid username
        }
        else {
            return { 'validateUsername': true }; // Return as invalid username
        }
    };
    SignupComponent.prototype.onRegisterSubmit = function () {
        var _this = this;
        var userobj = {
            name: this.form.get('username').value,
            email: this.form.get('email').value,
            password: this.form.get('password').value,
        };
        console.log(userobj);
        this.service.registerUser(userobj).subscribe(function (data) {
            if (data.code == "successful") {
                console.log(data);
                _this.flashMessages.show('Successfully Registered!', { cssClass: 'alert-success', timeout: 2000 });
                setTimeout(function () {
                    _this.flashMessages.show('Request is pending for approval by admin', { cssClass: 'alert-warning', timeout: 3000 });
                }, 2000);
                _this.form.reset();
                setTimeout(function () {
                    _this.router.navigate(['login']);
                }, 2000);
            }
            else {
                _this.flashMessages.show(data.message, { cssClass: 'alert-danger', timeout: 4000 });
            }
        });
    };
    SignupComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-signup',
            template: __webpack_require__(/*! ./signup.component.html */ "./src/app/signup/signup.component.html"),
            styles: [__webpack_require__(/*! ./signup.component.scss */ "./src/app/signup/signup.component.scss")],
            animations: [Object(_router_animations__WEBPACK_IMPORTED_MODULE_1__["routerTransition"])()]
        }),
        __metadata("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"],
            angular2_flash_messages__WEBPACK_IMPORTED_MODULE_4__["FlashMessagesService"],
            _shared_services_bank_service__WEBPACK_IMPORTED_MODULE_3__["BankService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"]])
    ], SignupComponent);
    return SignupComponent;
}());



/***/ }),

/***/ "./src/app/signup/signup.module.ts":
/*!*****************************************!*\
  !*** ./src/app/signup/signup.module.ts ***!
  \*****************************************/
/*! exports provided: SignupModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SignupModule", function() { return SignupModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _signup_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./signup-routing.module */ "./src/app/signup/signup-routing.module.ts");
/* harmony import */ var _signup_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./signup.component */ "./src/app/signup/signup.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var SignupModule = /** @class */ (function () {
    function SignupModule() {
    }
    SignupModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _signup_routing_module__WEBPACK_IMPORTED_MODULE_3__["SignupRoutingModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["ReactiveFormsModule"]
            ],
            declarations: [_signup_component__WEBPACK_IMPORTED_MODULE_4__["SignupComponent"]]
        })
    ], SignupModule);
    return SignupModule;
}());



/***/ })

}]);
//# sourceMappingURL=signup-signup-module.js.map