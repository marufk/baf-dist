(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["common"],{

/***/ "./node_modules/ngx-order-pipe/ngx-order-pipe.es5.js":
/*!***********************************************************!*\
  !*** ./node_modules/ngx-order-pipe/ngx-order-pipe.es5.js ***!
  \***********************************************************/
/*! exports provided: OrderPipe, OrderModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrderPipe", function() { return OrderPipe; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrderModule", function() { return OrderModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");

var OrderPipe = /** @class */ (function () {
    function OrderPipe() {
    }
    /**
     * Check if a value is a string
     *
     * @param {?} value
     * @return {?}
     */
    OrderPipe.isString = function (value) {
        return typeof value === 'string' || value instanceof String;
    };
    /**
     * Sorts values ignoring the case
     *
     * @param {?} a
     * @param {?} b
     * @return {?}
     */
    OrderPipe.caseInsensitiveSort = function (a, b) {
        if (OrderPipe.isString(a) && OrderPipe.isString(b)) {
            return a.localeCompare(b);
        }
        return OrderPipe.defaultCompare(a, b);
    };
    /**
     * Default compare method
     *
     * @param {?} a
     * @param {?} b
     * @return {?}
     */
    OrderPipe.defaultCompare = function (a, b) {
        if (a === b) {
            return 0;
        }
        if (a == null) {
            return 1;
        }
        if (b == null) {
            return -1;
        }
        return a > b ? 1 : -1;
    };
    /**
     * Parse expression, split into items
     * @param {?} expression
     * @return {?}
     */
    OrderPipe.parseExpression = function (expression) {
        expression = expression.replace(/\[(\w+)\]/g, '.$1');
        expression = expression.replace(/^\./, '');
        return expression.split('.');
    };
    /**
     * Get value by expression
     *
     * @param {?} object
     * @param {?} expression
     * @return {?}
     */
    OrderPipe.getValue = function (object, expression) {
        for (var /** @type {?} */ i = 0, /** @type {?} */ n = expression.length; i < n; ++i) {
            var /** @type {?} */ k = expression[i];
            if (!(k in object)) {
                return;
            }
            if (typeof object[k] === 'function') {
                object = object[k]();
            }
            else {
                object = object[k];
            }
        }
        return object;
    };
    /**
     * Set value by expression
     *
     * @param {?} object
     * @param {?} value
     * @param {?} expression
     * @return {?}
     */
    OrderPipe.setValue = function (object, value, expression) {
        var /** @type {?} */ i;
        for (i = 0; i < expression.length - 1; i++) {
            object = object[expression[i]];
        }
        object[expression[i]] = value;
    };
    /**
     * @param {?} value
     * @param {?=} expression
     * @param {?=} reverse
     * @param {?=} isCaseInsensitive
     * @param {?=} comparator
     * @return {?}
     */
    OrderPipe.prototype.transform = function (value, expression, reverse, isCaseInsensitive, comparator) {
        if (isCaseInsensitive === void 0) { isCaseInsensitive = false; }
        if (!value) {
            return value;
        }
        if (Array.isArray(expression)) {
            return this.multiExpressionTransform(value, expression, reverse, isCaseInsensitive, comparator);
        }
        if (Array.isArray(value)) {
            return this.sortArray(value.slice(), expression, reverse, isCaseInsensitive, comparator);
        }
        if (typeof value === 'object') {
            return this.transformObject(Object.assign({}, value), expression, reverse, isCaseInsensitive, comparator);
        }
        return value;
    };
    /**
     * Sort array
     *
     * @param {?} value
     * @param {?=} expression
     * @param {?=} reverse
     * @param {?=} isCaseInsensitive
     * @param {?=} comparator
     * @return {?}
     */
    OrderPipe.prototype.sortArray = function (value, expression, reverse, isCaseInsensitive, comparator) {
        var /** @type {?} */ isDeepLink = expression && expression.indexOf('.') !== -1;
        if (isDeepLink) {
            expression = OrderPipe.parseExpression(expression);
        }
        var /** @type {?} */ compareFn;
        if (comparator && typeof comparator === 'function') {
            compareFn = comparator;
        }
        else {
            compareFn = isCaseInsensitive ? OrderPipe.caseInsensitiveSort : OrderPipe.defaultCompare;
        }
        var /** @type {?} */ array = value.sort(function (a, b) {
            if (!expression) {
                return compareFn(a, b);
            }
            if (!isDeepLink) {
                if (a && b) {
                    return compareFn(a[expression], b[expression]);
                }
                return compareFn(a, b);
            }
            return compareFn(OrderPipe.getValue(a, expression), OrderPipe.getValue(b, expression));
        });
        if (reverse) {
            return array.reverse();
        }
        return array;
    };
    /**
     * Transform Object
     *
     * @param {?} value
     * @param {?=} expression
     * @param {?=} reverse
     * @param {?=} isCaseInsensitive
     * @param {?=} comparator
     * @return {?}
     */
    OrderPipe.prototype.transformObject = function (value, expression, reverse, isCaseInsensitive, comparator) {
        var /** @type {?} */ parsedExpression = OrderPipe.parseExpression(expression);
        var /** @type {?} */ lastPredicate = parsedExpression.pop();
        var /** @type {?} */ oldValue = OrderPipe.getValue(value, parsedExpression);
        if (!Array.isArray(oldValue)) {
            parsedExpression.push(lastPredicate);
            lastPredicate = null;
            oldValue = OrderPipe.getValue(value, parsedExpression);
        }
        if (!oldValue) {
            return value;
        }
        OrderPipe.setValue(value, this.transform(oldValue, lastPredicate, reverse, isCaseInsensitive), parsedExpression);
        return value;
    };
    /**
     * Apply multiple expressions
     *
     * @param {?} value
     * @param {?} expressions
     * @param {?} reverse
     * @param {?=} isCaseInsensitive
     * @param {?=} comparator
     * @return {?}
     */
    OrderPipe.prototype.multiExpressionTransform = function (value, expressions, reverse, isCaseInsensitive, comparator) {
        var _this = this;
        if (isCaseInsensitive === void 0) { isCaseInsensitive = false; }
        return expressions.reverse().reduce(function (result, expression) {
            return _this.transform(result, expression, reverse, isCaseInsensitive, comparator);
        }, value);
    };
    return OrderPipe;
}());
OrderPipe.decorators = [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Pipe"], args: [{
                name: 'orderBy',
                pure: false
            },] },
];
/**
 * @nocollapse
 */
OrderPipe.ctorParameters = function () { return []; };
/**
 * Created by vadimdez on 20/01/2017.
 */
var OrderModule = /** @class */ (function () {
    function OrderModule() {
    }
    return OrderModule;
}());
OrderModule.decorators = [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"], args: [{
                declarations: [OrderPipe],
                exports: [OrderPipe],
                providers: [OrderPipe]
            },] },
];
/**
 * @nocollapse
 */
OrderModule.ctorParameters = function () { return []; };
/**
 * Generated bundle index. Do not edit.
 */

//# sourceMappingURL=ngx-order-pipe.es5.js.map


/***/ }),

/***/ "./node_modules/ngx-password-toggle/ngx-password-toggle.umd.js":
/*!*********************************************************************!*\
  !*** ./node_modules/ngx-password-toggle/ngx-password-toggle.umd.js ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

(function (global, factory) {
	 true ? factory(exports, __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js"), __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js")) :
	undefined;
}(this, (function (exports,core,common) { 'use strict';

var eye = 'data:image/svg+xml;utf8;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pgo8IS0tIEdlbmVyYXRvcjogQWRvYmUgSWxsdXN0cmF0b3IgMTkuMC4wLCBTVkcgRXhwb3J0IFBsdWctSW4gLiBTVkcgVmVyc2lvbjogNi4wMCBCdWlsZCAwKSAgLS0+CjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgdmVyc2lvbj0iMS4xIiBpZD0iQ2FwYV8xIiB4PSIwcHgiIHk9IjBweCIgdmlld0JveD0iMCAwIDQ4OC44NSA0ODguODUiIHN0eWxlPSJlbmFibGUtYmFja2dyb3VuZDpuZXcgMCAwIDQ4OC44NSA0ODguODU7IiB4bWw6c3BhY2U9InByZXNlcnZlIiB3aWR0aD0iMTZweCIgaGVpZ2h0PSIxNnB4Ij4KPGc+Cgk8cGF0aCBkPSJNMjQ0LjQyNSw5OC43MjVjLTkzLjQsMC0xNzguMSw1MS4xLTI0MC42LDEzNC4xYy01LjEsNi44LTUuMSwxNi4zLDAsMjMuMWM2Mi41LDgzLjEsMTQ3LjIsMTM0LjIsMjQwLjYsMTM0LjIgICBzMTc4LjEtNTEuMSwyNDAuNi0xMzQuMWM1LjEtNi44LDUuMS0xNi4zLDAtMjMuMUM0MjIuNTI1LDE0OS44MjUsMzM3LjgyNSw5OC43MjUsMjQ0LjQyNSw5OC43MjV6IE0yNTEuMTI1LDM0Ny4wMjUgICBjLTYyLDMuOS0xMTMuMi00Ny4yLTEwOS4zLTEwOS4zYzMuMi01MS4yLDQ0LjctOTIuNyw5NS45LTk1LjljNjItMy45LDExMy4yLDQ3LjIsMTA5LjMsMTA5LjMgICBDMzQzLjcyNSwzMDIuMjI1LDMwMi4yMjUsMzQzLjcyNSwyNTEuMTI1LDM0Ny4wMjV6IE0yNDguMDI1LDI5OS42MjVjLTMzLjQsMi4xLTYxLTI1LjQtNTguOC01OC44YzEuNy0yNy42LDI0LjEtNDkuOSw1MS43LTUxLjcgICBjMzMuNC0yLjEsNjEsMjUuNCw1OC44LDU4LjhDMjk3LjkyNSwyNzUuNjI1LDI3NS41MjUsMjk3LjkyNSwyNDguMDI1LDI5OS42MjV6IiBmaWxsPSIjMDAwMDAwIi8+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPC9zdmc+Cg==';
var eyeCrossed = 'data:image/svg+xml;utf8;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pgo8IS0tIEdlbmVyYXRvcjogQWRvYmUgSWxsdXN0cmF0b3IgMTcuMS4wLCBTVkcgRXhwb3J0IFBsdWctSW4gLiBTVkcgVmVyc2lvbjogNi4wMCBCdWlsZCAwKSAgLS0+CjwhRE9DVFlQRSBzdmcgUFVCTElDICItLy9XM0MvL0RURCBTVkcgMS4xLy9FTiIgImh0dHA6Ly93d3cudzMub3JnL0dyYXBoaWNzL1NWRy8xLjEvRFREL3N2ZzExLmR0ZCI+CjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgdmVyc2lvbj0iMS4xIiBpZD0iQ2FwYV8xIiB4PSIwcHgiIHk9IjBweCIgdmlld0JveD0iMCAwIDM3OS41NjEgMzc5LjU2MSIgc3R5bGU9ImVuYWJsZS1iYWNrZ3JvdW5kOm5ldyAwIDAgMzc5LjU2MSAzNzkuNTYxOyIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSIgd2lkdGg9IjE2cHgiIGhlaWdodD0iMTZweCI+CjxwYXRoIGQ9Ik0zMjAuODM4LDEyMS45MjljLTcuMzIxLTUuNjc3LTE1LjYwNC0xMS42MS0yNC42NjQtMTcuMzYyTDM0MS40OTgsNTguNmM2LjIwNC02LjI5Miw2LjEzMy0xNi40MjMtMC4xNi0yMi42MjcgIGMtNi4yOTItNi4yMDQtMTYuNDIyLTYuMTM0LTIyLjYyNiwwLjE1OUwyNjcuMTIsODguNDU2Yy0yMy4yNTMtMTEuMDE4LTQ5LjU4Ny0xOC45NjMtNzcuMTkyLTE4Ljk2M2wtMC45OTEsMC4wMDMgIGMtNTIuMjYyLDAuMzY5LTk5Ljk4MSwyOS45MzItMTMwLjgxLDU0LjY2N0MyNy42MzIsMTQ4LjYzMS0wLjA3MywxODAuMzIsMCwxOTAuNjQ5YzAuMDgyLDExLjcwMiwyNi45ODEsNDIuMjM0LDU4LjczNyw2Ni42NzEgIGM2LjY0Niw1LjExNCwxNS4wNzMsMTEuMTQ1LDI0Ljg3NSwxNy4yNDlMMzcuODcsMzIwLjk2MmMtNi4yMDQsNi4yOTItNi4xMzMsMTYuNDIzLDAuMTYsMjIuNjI3ICBjMy4xMTcsMy4wNzQsNy4xNzUsNC42MDcsMTEuMjMzLDQuNjA3YzQuMTMyLDAsOC4yNjMtMS41OTEsMTEuMzk0LTQuNzY2bDUyLjIxLTUyLjk1MWMyMi42NjYsMTAuNTM1LDQ5LjEwMSwxOC42NTEsNzYuNzUyLDE4LjY1MSAgbDEuMDA3LTAuMDAzYzU1LjUyNS0wLjM5MiwxMDUuNzc2LTMzLjU2NiwxMzAuNzg4LTUzLjMwN2MzMS42MDMtMjQuOTQyLDU4LjIzMS01Ni4wMTEsNTguMTQ4LTY3Ljg0NSAgQzM3OS40ODQsMTc2Ljk4OCwzNTIuMDQzLDE0Ni4xMjYsMzIwLjgzOCwxMjEuOTI5eiBNMTExLjMxNCwxODkuODY0Yy0wLjMwNS00My4zMzYsMzQuNTc4LTc4LjcxNCw3Ny45MTMtNzkuMDE5ICBjMTUuOTc1LTAuMTEzLDMwLjg2LDQuNTY5LDQzLjMxNCwxMi42ODFsLTEwNy45MzgsMTA5LjQ3QzExNi4zMTgsMjIwLjY1NywxMTEuNDI3LDIwNS44MzksMTExLjMxNCwxODkuODY0eiBNMTkwLjMzNCwyNjcuNzc3ICBjLTE1Ljc4NCwwLjExMS0zMC41LTQuNDYyLTQyLjg2My0xMi4zOTZsMTA3Ljc3Ny0xMDkuMzA4YzguMTA3LDEyLjI1LDEyLjg4NywyNi45LDEyLjk5OSw0Mi42ODQgIEMyNjguNTUyLDIzMi4wOTQsMjMzLjY2OSwyNjcuNDcyLDE5MC4zMzQsMjY3Ljc3N3oiIGZpbGw9IiMwMDAwMDAiLz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPC9zdmc+Cg==';
var NgxPasswordToggleDirective = (function () {
    /**
     * @param {?} elem
     * @param {?} renderer
     */
    function NgxPasswordToggleDirective(elem, renderer) {
        this.elem = elem;
        this.renderer = renderer;
        this.visible = false;
    }
    /**
     * @return {?}
     */
    NgxPasswordToggleDirective.prototype.ngOnInit = function () {
        var _this = this;
        var /** @type {?} */ inputElem = this.elem.nativeElement;
        var /** @type {?} */ parentElem = inputElem.parentElement;
        this.renderer.setStyle(parentElem, 'position', 'relative');
        var /** @type {?} */ inputElemHeight = inputElem.offsetHeight;
        var /** @type {?} */ inputElemTop = inputElem.offsetTop;
        this.renderer.setStyle(inputElem, 'padding-right', (inputElemHeight + 5) + 'px');
        // create toggle button
        var /** @type {?} */ toggleElem = this.renderer.createElement('div');
        this.renderer.setStyle(toggleElem, 'position', 'absolute');
        this.renderer.setStyle(toggleElem, 'top', inputElemTop + 'px');
        this.renderer.setStyle(toggleElem, 'right', '0px');
        this.renderer.setStyle(toggleElem, 'width', inputElemHeight + 'px');
        this.renderer.setStyle(toggleElem, 'height', inputElemHeight + 'px');
        this.renderer.setStyle(toggleElem, 'cursor', 'pointer');
        this.renderer.setStyle(toggleElem, 'opacity', '0.6');
        this.renderer.setStyle(toggleElem, 'background-color', 'transparent');
        this.renderer.setStyle(toggleElem, 'background-repeat', 'no-repeat');
        this.renderer.setStyle(toggleElem, 'background-position', 'center center');
        this.renderer.setStyle(toggleElem, 'background-image', "url(" + eyeCrossed + ")");
        // append toggle element to input element holder
        this.renderer.appendChild(parentElem, toggleElem);
        // listen to click event on toggle button
        this.renderer.listen(toggleElem, 'click', function (event) {
            _this.visible = !_this.visible;
            _this.renderer.setStyle(toggleElem, 'background-image', "url(" + (_this.visible ? eye : eyeCrossed) + ")");
            _this.renderer.setAttribute(inputElem, 'type', _this.visible ? 'text' : 'password');
        });
    };
    return NgxPasswordToggleDirective;
}());
NgxPasswordToggleDirective.decorators = [
    { type: core.Directive, args: [{
                selector: '[passwordToggle]'
            },] },
];
/**
 * @nocollapse
 */
NgxPasswordToggleDirective.ctorParameters = function () { return [
    { type: core.ElementRef, },
    { type: core.Renderer2, },
]; };

var NgxPasswordToggleModule = (function () {
    function NgxPasswordToggleModule() {
    }
    return NgxPasswordToggleModule;
}());
NgxPasswordToggleModule.decorators = [
    { type: core.NgModule, args: [{
                imports: [
                    common.CommonModule
                ],
                declarations: [
                    NgxPasswordToggleDirective
                ],
                exports: [
                    NgxPasswordToggleDirective
                ]
            },] },
];
/**
 * @nocollapse
 */
NgxPasswordToggleModule.ctorParameters = function () { return []; };

exports.NgxPasswordToggleModule = NgxPasswordToggleModule;

Object.defineProperty(exports, '__esModule', { value: true });

})));


/***/ }),

/***/ "./src/app/router.animations.ts":
/*!**************************************!*\
  !*** ./src/app/router.animations.ts ***!
  \**************************************/
/*! exports provided: routerTransition, noTransition, slideToRight, slideToLeft, slideToBottom, slideToTop */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "routerTransition", function() { return routerTransition; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "noTransition", function() { return noTransition; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "slideToRight", function() { return slideToRight; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "slideToLeft", function() { return slideToLeft; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "slideToBottom", function() { return slideToBottom; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "slideToTop", function() { return slideToTop; });
/* harmony import */ var _angular_animations__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/animations */ "./node_modules/@angular/animations/fesm5/animations.js");

function routerTransition() {
    return slideToBottom();
}
function noTransition() {
    return Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["trigger"])('routerTransition', []);
}
function slideToRight() {
    return Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["trigger"])('routerTransition', [
        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["state"])('void', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({})),
        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["state"])('*', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({})),
        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["transition"])(':enter', [
            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({ transform: 'translateX(-100%)' }),
            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["animate"])('0.5s ease-in-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({ transform: 'translateX(0%)' }))
        ]),
        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["transition"])(':leave', [
            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({ transform: 'translateX(0%)' }),
            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["animate"])('0.5s ease-in-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({ transform: 'translateX(100%)' }))
        ])
    ]);
}
function slideToLeft() {
    return Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["trigger"])('routerTransition', [
        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["state"])('void', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({})),
        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["state"])('*', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({})),
        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["transition"])(':enter', [
            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({ transform: 'translateX(100%)' }),
            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["animate"])('0.5s ease-in-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({ transform: 'translateX(0%)' }))
        ]),
        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["transition"])(':leave', [
            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({ transform: 'translateX(0%)' }),
            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["animate"])('0.5s ease-in-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({ transform: 'translateX(-100%)' }))
        ])
    ]);
}
function slideToBottom() {
    return Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["trigger"])('routerTransition', [
        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["state"])('void', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({})),
        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["state"])('*', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({})),
        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["transition"])(':enter', [
            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({ transform: 'translateY(-100%)' }),
            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["animate"])('0.5s ease-in-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({ transform: 'translateY(0%)' }))
        ]),
        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["transition"])(':leave', [
            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({ transform: 'translateY(0%)' }),
            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["animate"])('0.5s ease-in-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({ transform: 'translateY(100%)' }))
        ])
    ]);
}
function slideToTop() {
    return Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["trigger"])('routerTransition', [
        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["state"])('void', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({})),
        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["state"])('*', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({})),
        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["transition"])(':enter', [
            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({ transform: 'translateY(100%)' }),
            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["animate"])('0.5s ease-in-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({ transform: 'translateY(0%)' }))
        ]),
        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["transition"])(':leave', [
            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({ transform: 'translateY(0%)' }),
            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["animate"])('0.5s ease-in-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({ transform: 'translateY(-100%)' }))
        ])
    ]);
}


/***/ })

}]);
//# sourceMappingURL=common.js.map