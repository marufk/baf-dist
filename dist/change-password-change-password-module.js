(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["change-password-change-password-module"],{

/***/ "./src/app/layout/change-password/change-password-routing.module.ts":
/*!**************************************************************************!*\
  !*** ./src/app/layout/change-password/change-password-routing.module.ts ***!
  \**************************************************************************/
/*! exports provided: ChangePasswordRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChangePasswordRoutingModule", function() { return ChangePasswordRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _change_password_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./change-password.component */ "./src/app/layout/change-password/change-password.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var routes = [
    {
        path: '',
        component: _change_password_component__WEBPACK_IMPORTED_MODULE_2__["ChangePasswordComponent"]
    }
];
var ChangePasswordRoutingModule = /** @class */ (function () {
    function ChangePasswordRoutingModule() {
    }
    ChangePasswordRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], ChangePasswordRoutingModule);
    return ChangePasswordRoutingModule;
}());



/***/ }),

/***/ "./src/app/layout/change-password/change-password.component.html":
/*!***********************************************************************!*\
  !*** ./src/app/layout/change-password/change-password.component.html ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"login-page\" [@routerTransition]>\n  <div class=\"row justify-content-md-center\">\n      <div class=\"col-md-4 crytal-black\">\n          <i class=\"fas fa-unlock-alt\" style=\"font-size:40px;\"></i>\n          <!-- <img class=\"user-avatar\" src=\"assets/images/BA-logo.png\" width=\"70px\" /> -->\n          <h1 class=\"card-title\">Change Password</h1>\n          <form method=\"post\" role=\"form\" [formGroup]=\"form\" (submit)=\"OnChangePassword()\">\n              <div class=\"form-content\">\n                  <div class=\"form-group\">\n                      <div [ngClass]=\"{'has-error': (form.controls.oldpassword.errors && form.controls.oldpassword.dirty), 'has-success': !form.controls.oldpassword.errors && oldpasswordValid}\">\n                          <input type=\"password\" class=\"form-control input-underline input-lg\" passwordToggle placeholder=\"Old Password\"\n                              name=\"oldpassword\" formControlName=\"oldpassword\" >\n                          <ul class=\"help-block text-white\">\n                              <li *ngIf=\"form.controls.oldpassword.errors?.required && form.controls.oldpassword.dirty && form.controls.oldpassword.touched\">This\n                                  field is required</li>\n                              <li *ngIf=\"form.controls.oldpassword.errors?.minlength && form.controls.oldpassword.dirty || form.controls.oldpassword.errors?.maxlength && form.controls.oldpassword.dirty \">Minimum\n                                  characters: 6, Maximum characters: 16</li>\n                              <!-- <li *ngIf=\"form.controls.oldpassword.errors?.validateUsername && form.controls.oldpassword.dirty\">Username\n                                  must not have any special characters</li> -->\n                          </ul>\n                      </div>\n                  </div>\n\n                  <div class=\"form-group\">\n                      <div [ngClass]=\"{'has-error': (form.controls.password.errors && form.controls.password.dirty), 'has-success': !form.controls.password.errors}\">\n                          <input type=\"password\" class=\"form-control input-underline input-lg\" passwordToggle placeholder=\"New Password\"\n                              name=\"password\" formControlName=\"password\">\n                          <ul class=\"help-block text-white\">\n                              <li *ngIf=\"form.controls.password.errors?.required && form.controls.password.dirty\">This\n                                  field is required</li>\n                              <li *ngIf=\"form.controls.password.errors?.minlength && form.controls.password.dirty || form.controls.password.errors?.maxlength && form.controls.password.dirty \">Minimum\n                                  characters: 6, Maximum characters: 16</li>\n                              <li *ngIf=\"form.controls.password.errors?.validatePassword && form.controls.password.dirty\">Password\n                                  must be at least 6 characters but not more than 16</li>\n                          </ul>\n                      </div>\n                  </div>\n\n                  <div class=\"form-group\">\n                      <div [ngClass]=\"{'has-error': (form.controls.confirmpass.errors && form.controls.confirmpass.dirty) || (form.errors?.matchingPasswords && form.controls.confirmpass.dirty), 'has-success': !form.controls.confirmpass.errors && !form.errors?.matchingPasswords}\">\n                          <input type=\"password\" class=\"form-control input-underline input-lg\" passwordToggle placeholder=\"Confirm Password\"\n                              name=\"confirmpass\" formControlName=\"confirmpass\">\n                          <ul class=\"help-block text-white\">\n                              <li *ngIf=\"form.controls.confirmpass.errors?.required && form.controls.confirmpass.dirty\">This\n                                  field is required</li>\n                              <li *ngIf=\"form.errors?.matchingPasswords && form.controls.confirmpass.dirty\">Password\n                                  do not match</li>\n                          </ul>\n                      </div>\n                  </div>\n              </div>\n              <button class=\"btn rounded-btn\" type=\"reset\" > Clear </button>&nbsp;&nbsp;\n              <button class=\"btn rounded-btn\" type=\"submit\" [disabled]=\"!form.valid\"> Save </button>&nbsp;\n              <!-- <a class=\"btn rounded-btn\" [routerLink]=\"['/login']\"> Log in </a> -->\n          </form>\n      </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/layout/change-password/change-password.component.scss":
/*!***********************************************************************!*\
  !*** ./src/app/layout/change-password/change-password.component.scss ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ":host {\n  display: block; }\n\n.login-page {\n  top: 0;\n  left: 0;\n  right: 0;\n  bottom: 0;\n  text-align: center;\n  color: #fff;\n  padding: 1em; }\n\n.login-page .crytal-black {\n    background: rgba(0, 0, 0, 0.658824);\n    padding: 25px; }\n\n.login-page .crytal-black .bottom {\n      padding-top: 8px;\n      font-size: 14px; }\n\n.login-page .crytal-black .bottom p {\n        margin-bottom: 0rem; }\n\n.login-page .crytal-black .bottom a {\n        color: #ced4da;\n        text-decoration: none; }\n\n.login-page .crytal-black .bottom a:hover {\n          color: #bc2418; }\n\n.login-page .crytal-black .forgot-pass {\n      float: right;\n      margin-top: -55px;\n      font-size: 13px; }\n\n.login-page .crytal-black .forgot-pass a {\n        color: #ced4da;\n        text-decoration: none; }\n\n.login-page .crytal-black .forgot-pass a:hover {\n          color: #bc2418; }\n\n.login-page .col-lg-4 {\n    padding: 0; }\n\n.login-page .input-lg {\n    height: 46px;\n    padding: 10px 16px;\n    font-size: 18px;\n    line-height: 1.3333333;\n    border-radius: 0; }\n\n.login-page .input-underline {\n    background: 0 0;\n    border: none;\n    box-shadow: none;\n    border-bottom: 2px solid rgba(255, 255, 255, 0.5);\n    color: #fff;\n    border-radius: 0; }\n\n.login-page .input-underline:focus {\n    border-bottom: 2px solid #fff;\n    box-shadow: none; }\n\n.login-page .rounded-btn {\n    border-radius: 50px;\n    color: rgba(255, 255, 255, 0.8);\n    background: #222;\n    border: 2px solid rgba(255, 255, 255, 0.8);\n    font-size: 18px;\n    line-height: 40px;\n    padding: 0 25px; }\n\n.login-page .rounded-btn:hover,\n  .login-page .rounded-btn:focus,\n  .login-page .rounded-btn:active,\n  .login-page .rounded-btn:visited {\n    color: white;\n    border: 2px solid white;\n    outline: none; }\n\n.login-page h1 {\n    font-weight: 300;\n    margin-top: 20px;\n    margin-bottom: 10px;\n    font-size: 36px; }\n\n.login-page h1 small {\n      color: rgba(255, 255, 255, 0.7); }\n\n.login-page .form-group {\n    padding: 8px 0; }\n\n.login-page .form-group input::-webkit-input-placeholder {\n      color: rgba(255, 255, 255, 0.6) !important; }\n\n.login-page .form-group input:-moz-placeholder {\n      /* Firefox 18- */\n      color: rgba(255, 255, 255, 0.6) !important; }\n\n.login-page .form-group input::-moz-placeholder {\n      /* Firefox 19+ */\n      color: rgba(255, 255, 255, 0.6) !important; }\n\n.login-page .form-group input:-ms-input-placeholder {\n      color: rgba(255, 255, 255, 0.6) !important; }\n\n.login-page .form-content {\n    padding: 40px 0; }\n\n.login-page .user-avatar {\n    -webkit-border-radius: 50%; }\n\n@media (min-width: 768px) and (max-width: 1200px) {\n    .login-page .col-md-4 {\n      flex: 0 0 42.333333%;\n      max-width: 42.333333%; } }\n\n.input-underline.ng-invalid.ng-touched, select.ng-invalid.ng-touched {\n  border-bottom: 2px solid red; }\n\n.help-block {\n  font-size: 14px;\n  float: left;\n  text-align: left; }\n"

/***/ }),

/***/ "./src/app/layout/change-password/change-password.component.ts":
/*!*********************************************************************!*\
  !*** ./src/app/layout/change-password/change-password.component.ts ***!
  \*********************************************************************/
/*! exports provided: ChangePasswordComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChangePasswordComponent", function() { return ChangePasswordComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _router_animations__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../router.animations */ "./src/app/router.animations.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _shared_services_bank_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../shared/services/bank.service */ "./src/app/shared/services/bank.service.ts");
/* harmony import */ var angular2_flash_messages__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! angular2-flash-messages */ "./node_modules/angular2-flash-messages/module/index.js");
/* harmony import */ var angular2_flash_messages__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(angular2_flash_messages__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var ChangePasswordComponent = /** @class */ (function () {
    function ChangePasswordComponent(formBuilder, flashMessages, service, router) {
        this.formBuilder = formBuilder;
        this.flashMessages = flashMessages;
        this.service = service;
        this.router = router;
    }
    ChangePasswordComponent.prototype.ngOnInit = function () {
        this.createForm();
    };
    ChangePasswordComponent.prototype.createForm = function () {
        this.form = this.formBuilder.group({
            // firstname Input
            oldpassword: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required,
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].minLength(6),
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].maxLength(16),
                ])],
            // Password Input
            password: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required,
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].minLength(6),
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].maxLength(16),
                ])],
            // Confirm Password Input
            confirmpass: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required] // Field is required
        }, {
            validator: this.matchingPasswords('password', 'confirmpass')
        });
    };
    // Funciton to ensure passwords match
    ChangePasswordComponent.prototype.matchingPasswords = function (password, confirmpass) {
        return function (group) {
            // Check if both fields are the same
            if (group.controls[password].value === group.controls[confirmpass].value) {
                return null; // Return as a match
            }
            else {
                return { 'matchingPasswords': true }; // Return as error: do not match
            }
        };
    };
    ChangePasswordComponent.prototype.OnChangePassword = function () {
        var _this = this;
        var email = this.service.getEmail();
        var res = {
            email: email,
            oldpassword: this.form.get('oldpassword').value,
            newpassword: this.form.get('password').value,
        };
        console.log(res);
        this.service.ChangePasswordByUser(res).subscribe(function (data) {
            if (data.code == "successful") {
                _this.flashMessages.show('Password Changed!', { cssClass: 'alert-success', timeout: 2000 });
                setTimeout(function () {
                    _this.flashMessages.show('You will be Redirect to Login Page!', { cssClass: 'alert-warning', timeout: 3500 });
                }, 1000);
                setTimeout(function () {
                    _this.service.RemoveToken();
                }, 3000);
                console.log(data);
            }
            else {
                _this.flashMessages.show(data.message, { cssClass: 'alert-danger', timeout: 2000 });
            }
        });
        this.form.reset();
    };
    ChangePasswordComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-change-password',
            template: __webpack_require__(/*! ./change-password.component.html */ "./src/app/layout/change-password/change-password.component.html"),
            styles: [__webpack_require__(/*! ./change-password.component.scss */ "./src/app/layout/change-password/change-password.component.scss")],
            animations: [Object(_router_animations__WEBPACK_IMPORTED_MODULE_1__["routerTransition"])()]
        }),
        __metadata("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"],
            angular2_flash_messages__WEBPACK_IMPORTED_MODULE_4__["FlashMessagesService"],
            _shared_services_bank_service__WEBPACK_IMPORTED_MODULE_3__["BankService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"]])
    ], ChangePasswordComponent);
    return ChangePasswordComponent;
}());



/***/ }),

/***/ "./src/app/layout/change-password/change-password.module.ts":
/*!******************************************************************!*\
  !*** ./src/app/layout/change-password/change-password.module.ts ***!
  \******************************************************************/
/*! exports provided: ChangePasswordModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChangePasswordModule", function() { return ChangePasswordModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _change_password_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./change-password-routing.module */ "./src/app/layout/change-password/change-password-routing.module.ts");
/* harmony import */ var _change_password_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./change-password.component */ "./src/app/layout/change-password/change-password.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var ngx_password_toggle__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ngx-password-toggle */ "./node_modules/ngx-password-toggle/ngx-password-toggle.umd.js");
/* harmony import */ var ngx_password_toggle__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(ngx_password_toggle__WEBPACK_IMPORTED_MODULE_5__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var ChangePasswordModule = /** @class */ (function () {
    function ChangePasswordModule() {
    }
    ChangePasswordModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["ReactiveFormsModule"],
                _change_password_routing_module__WEBPACK_IMPORTED_MODULE_2__["ChangePasswordRoutingModule"],
                ngx_password_toggle__WEBPACK_IMPORTED_MODULE_5__["NgxPasswordToggleModule"]
            ],
            declarations: [_change_password_component__WEBPACK_IMPORTED_MODULE_3__["ChangePasswordComponent"]]
        })
    ], ChangePasswordModule);
    return ChangePasswordModule;
}());



/***/ })

}]);
//# sourceMappingURL=change-password-change-password-module.js.map