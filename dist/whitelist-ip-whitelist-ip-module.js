(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["whitelist-ip-whitelist-ip-module"],{

/***/ "./src/app/layout/whitelist-ip/whitelist-ip-routing.module.ts":
/*!********************************************************************!*\
  !*** ./src/app/layout/whitelist-ip/whitelist-ip-routing.module.ts ***!
  \********************************************************************/
/*! exports provided: WhitelistIpRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WhitelistIpRoutingModule", function() { return WhitelistIpRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _whitelist_ip_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./whitelist-ip.component */ "./src/app/layout/whitelist-ip/whitelist-ip.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var routes = [
    {
        path: '',
        component: _whitelist_ip_component__WEBPACK_IMPORTED_MODULE_2__["WhitelistIpComponent"]
    }
];
var WhitelistIpRoutingModule = /** @class */ (function () {
    function WhitelistIpRoutingModule() {
    }
    WhitelistIpRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], WhitelistIpRoutingModule);
    return WhitelistIpRoutingModule;
}());



/***/ }),

/***/ "./src/app/layout/whitelist-ip/whitelist-ip.component.html":
/*!*****************************************************************!*\
  !*** ./src/app/layout/whitelist-ip/whitelist-ip.component.html ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"card\" style=\"width: 20rem;\">\n  <div class=\"card-body\">\n    <h5 class=\"card-title\">Enter your IP Address</h5>\n    <form method=\"post\" [formGroup]=\"form\" (submit)=\"onIPSubmit()\">\n      <div class=\"form-group\">\n        <!-- <label for=\"exampleInputEmail1\">Enter IP Address</label> -->\n        <input type=\"text\" name=\"ip\" formControlName=\"ip\" class=\"form-control\" id=\"exampleInputEmail1\" aria-describedby=\"emailHelp\" placeholder=\"192.168.100.100\">\n        <ul class=\"help-block\">\n            <li *ngIf=\"form.controls.ip.errors?.required && form.controls.ip.dirty && form.controls.ip.touched\">This\n                field is required</li>\n            <li *ngIf=\"form.controls.ip.errors?.minlength && form.controls.ip.dirty || form.controls.ip.errors?.maxlength && form.controls.ip.dirty\">\n             This is not a format</li>\n            <li *ngIf=\"form.controls.ip.errors?.validateIP && form.controls.ip.dirty\">IP\n                must be Valid </li>\n        </ul>\n      </div>\n      <div style=\"text-align: center;\">\n          <button type=\"submit\" [disabled]=\"!form.valid\" class=\"btn btn-block btn-primary\">Add</button>\n      </div>\n    </form>\n  </div>\n</div>\n<hr>\n<ul *ngFor=\"let list of ips | orderBy:reverse | paginate: config\" class=\"list-group\">\n    <li class=\"list-group-item black-list\">{{ list.ip }}<button (click)=\"RemoveIP(list.ip)\" id=\"right\" type=\"button\" class=\"btn btn-outline-danger\">Delete</button></li>\n</ul>\n<pagination-controls *ngIf=\"ips\" class=\"my-pagination\" (pageChange)=\"pageChange($event)\"></pagination-controls>"

/***/ }),

/***/ "./src/app/layout/whitelist-ip/whitelist-ip.component.scss":
/*!*****************************************************************!*\
  !*** ./src/app/layout/whitelist-ip/whitelist-ip.component.scss ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".black-list {\n  background-color: #000000de;\n  border: 1px solid white;\n  color: white; }\n\n#right {\n  float: right; }\n\n.card {\n  background-color: #000000de;\n  color: white; }\n\n.my-pagination /deep/ .ngx-pagination {\n  margin-top: 20px;\n  background: #575757 !important; }\n"

/***/ }),

/***/ "./src/app/layout/whitelist-ip/whitelist-ip.component.ts":
/*!***************************************************************!*\
  !*** ./src/app/layout/whitelist-ip/whitelist-ip.component.ts ***!
  \***************************************************************/
/*! exports provided: WhitelistIpComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WhitelistIpComponent", function() { return WhitelistIpComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _shared_services_bank_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../shared/services/bank.service */ "./src/app/shared/services/bank.service.ts");
/* harmony import */ var angular2_flash_messages__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! angular2-flash-messages */ "./node_modules/angular2-flash-messages/module/index.js");
/* harmony import */ var angular2_flash_messages__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(angular2_flash_messages__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var WhitelistIpComponent = /** @class */ (function () {
    function WhitelistIpComponent(route, formBuilder, flashMessages, service, router) {
        var _this = this;
        this.route = route;
        this.formBuilder = formBuilder;
        this.flashMessages = flashMessages;
        this.service = service;
        this.router = router;
        this.createForm();
        this.config = {
            currentPage: 1,
            itemsPerPage: 5
        };
        this.route.queryParamMap
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["map"])(function (params) { return params.get('page'); }))
            .subscribe(function (page) { return _this.config.currentPage = page; });
    }
    WhitelistIpComponent.prototype.ngOnInit = function () {
        var _this = this;
        // const token = this.service.getToken();
        this.service.GetWhiteListIP().subscribe(function (data) {
            _this.ips = data;
            console.log(_this.ips);
        }, function (err) {
            if (err.status == 0 || err.status == 401) {
                _this.flashMessages.show('Session Expired Please Login again', { cssClass: 'alert-danger', timeout: 3000 });
                _this.service.RemoveToken();
                console.log('Session Expired');
            }
        });
    };
    WhitelistIpComponent.prototype.pageChange = function (newPage) {
        this.router.navigate(['whitelist-ip/'], { queryParams: { page: newPage } });
    };
    WhitelistIpComponent.prototype.createForm = function () {
        this.form = this.formBuilder.group({
            // IMEI Input
            ip: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].compose([
                    _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required,
                    _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(7),
                    _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].maxLength(15),
                    this.validateIP // Custom validation for IP address
                ])]
        });
    };
    // Function to validate IP in proper format
    WhitelistIpComponent.prototype.validateIP = function (controls) {
        var regExp = new RegExp('^((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$');
        if (regExp.test(controls.value)) {
            return null;
        }
        else {
            return { 'validateIP': true };
        }
    };
    WhitelistIpComponent.prototype.onIPSubmit = function () {
        var _this = this;
        var obj = {
            ip: this.form.get('ip').value
        };
        if (confirm("Are you sure you want to Add IP ?")) {
            // this.ip = this.form.get('ip').value;
            // const token = this.service.getToken();
            this.service.AddWhiteListIP(obj).subscribe(function (data) {
                if (data.code == "successful") {
                    _this.flashMessages.show('Ip Added!', { cssClass: 'alert-success', timeout: 2000 });
                    console.log(data);
                    setTimeout(function () {
                        window.location.reload();
                    }, 2000);
                }
                else {
                    _this.flashMessages.show('IP Already Added', { cssClass: 'alert-danger', timeout: 3000 });
                }
            });
        }
    };
    WhitelistIpComponent.prototype.RemoveIP = function (ip) {
        var _this = this;
        var obj = {
            ip: ip
        };
        if (confirm("Are you sure you want to Delete IP ?")) {
            console.log(obj);
            // const token = this.service.getToken();
            this.service.DeleteWhiteListIP(obj).subscribe(function (data) {
                _this.flashMessages.show('Ip Deleted!', { cssClass: 'alert-danger', timeout: 2000 });
                console.log(data);
            });
            setTimeout(function () {
                window.location.reload();
            }, 2000);
        }
    };
    WhitelistIpComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-whitelist-ip',
            template: __webpack_require__(/*! ./whitelist-ip.component.html */ "./src/app/layout/whitelist-ip/whitelist-ip.component.html"),
            styles: [__webpack_require__(/*! ./whitelist-ip.component.scss */ "./src/app/layout/whitelist-ip/whitelist-ip.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"],
            angular2_flash_messages__WEBPACK_IMPORTED_MODULE_3__["FlashMessagesService"],
            _shared_services_bank_service__WEBPACK_IMPORTED_MODULE_2__["BankService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]])
    ], WhitelistIpComponent);
    return WhitelistIpComponent;
}());



/***/ }),

/***/ "./src/app/layout/whitelist-ip/whitelist-ip.module.ts":
/*!************************************************************!*\
  !*** ./src/app/layout/whitelist-ip/whitelist-ip.module.ts ***!
  \************************************************************/
/*! exports provided: WhitelistIpModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WhitelistIpModule", function() { return WhitelistIpModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _my_pipe_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../my-pipe.module */ "./src/app/layout/my-pipe.module.ts");
/* harmony import */ var _whitelist_ip_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./whitelist-ip-routing.module */ "./src/app/layout/whitelist-ip/whitelist-ip-routing.module.ts");
/* harmony import */ var _whitelist_ip_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./whitelist-ip.component */ "./src/app/layout/whitelist-ip/whitelist-ip.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var ngx_order_pipe__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ngx-order-pipe */ "./node_modules/ngx-order-pipe/ngx-order-pipe.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







var WhitelistIpModule = /** @class */ (function () {
    function WhitelistIpModule() {
    }
    WhitelistIpModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_5__["ReactiveFormsModule"],
                _my_pipe_module__WEBPACK_IMPORTED_MODULE_2__["MyPipeModule"],
                ngx_order_pipe__WEBPACK_IMPORTED_MODULE_6__["OrderModule"],
                _whitelist_ip_routing_module__WEBPACK_IMPORTED_MODULE_3__["WhitelistIpRoutingModule"]
            ],
            declarations: [_whitelist_ip_component__WEBPACK_IMPORTED_MODULE_4__["WhitelistIpComponent"]]
        })
    ], WhitelistIpModule);
    return WhitelistIpModule;
}());



/***/ })

}]);
//# sourceMappingURL=whitelist-ip-whitelist-ip-module.js.map