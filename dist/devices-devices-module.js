(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["devices-devices-module"],{

/***/ "./src/app/layout/devices/devices-routing.module.ts":
/*!**********************************************************!*\
  !*** ./src/app/layout/devices/devices-routing.module.ts ***!
  \**********************************************************/
/*! exports provided: DevicesRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DevicesRoutingModule", function() { return DevicesRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _devices_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./devices.component */ "./src/app/layout/devices/devices.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var routes = [
    {
        path: '',
        component: _devices_component__WEBPACK_IMPORTED_MODULE_2__["DevicesComponent"]
    }
];
var DevicesRoutingModule = /** @class */ (function () {
    function DevicesRoutingModule() {
    }
    DevicesRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], DevicesRoutingModule);
    return DevicesRoutingModule;
}());



/***/ }),

/***/ "./src/app/layout/devices/devices.component.html":
/*!*******************************************************!*\
  !*** ./src/app/layout/devices/devices.component.html ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"form-group col-md-4 filterdiv\">\n  <input class=\"form-control\" [(ngModel)]=\"searchText\" placeholder=\"Filter By IMEI\">\n</div>\n<div class=\"table-responsive\">\n<table class=\"table table-hover\">\n    <thead>\n      <tr>\n        <th>#</th>\n        <th scope=\"col\">Device IMEI </th>\n        <th scope=\"col\">Device PAN</th>\n        <th scope=\"col\">TID</th>\n        <th scope=\"col\">Store ID</th>\n        <th scope=\"col\">Merchant Name</th>\n        <th scope=\"col\">SIM Number</th>\n        <th scope=\"col\">Configured</th>\n        <th scope=\"col\">Action </th>\n      </tr> \n    </thead>\n    <tbody>\n      <tr *ngFor=\"let device of devices | filter: searchText  | paginate:config let i= index\">\n        <td>{{i+1}}</td>\n        <td >{{device.imei}}</td>\n        <td>\n                {{device.pan[2].pan}}\n            <!-- <select class=\"custom-select\">\n                <option disabled selected>View PAN</option>\n                <option value=\"1\" >{{device.pan[2].pan}}</option>\n                <option value=\"1\" >{{device.pan[4].pan}}</option>\n            </select> -->\n        </td>\n        <td>{{device.tid}}</td>\n        <td>{{device.storeid}}</td>\n        <!-- <td>{{device.pan[0].pan}}</td> -->\n        <td>{{device.mn}}</td>\n        <td>{{device.sim}}</td>\n        <td>{{device.configured}}</td>\n        <td>\n          <button class=\"btn btn-outline-danger\" (click)=\"disablebutton(device)\">Disable</button>&nbsp;&nbsp;\n          <button class=\"btn btn-outline-primary\" (click)=\"open(content) || GetDevice(device)\">Update</button>        \n        </td>\n      </tr>\n    </tbody>\n  </table>\n  </div>\n  <pagination-controls *ngIf=\"devices\" class=\"my-pagination\" (pageChange)=\"pageChange($event)\"></pagination-controls>\n<!-- UPDATE MODAL -->\n<ng-template #content let-modal let-c=\"close\" let-d=\"dismiss\">\n    <div class=\"modal-header\">\n        <h4 class=\"modal-title\" id=\"modal-basic-title\">Update Device</h4>\n        <button type=\"button\" class=\"close\" aria-label=\"Close\" (click)=\"d('Cross click')\">\n            <span aria-hidden=\"true\">&times;</span>\n        </button>\n    </div>\n    <div class=\"modal-body\">\n            <form method=\"post\" role=\"form\" [formGroup]=\"form\">\n                        <!-- <div class=\"form-group\">\n                            <div [ngClass]=\"{'has-error': (form.controls.imei.errors && form.controls.imei.dirty), 'has-success': !form.controls.imei.errors && imeiValid}\">\n                                <input [(ngModel)]=\"oldimei\" type=\"text\" class=\"form-control input-underline input-lg\" placeholder=\"IMEI : 358472042445412\"\n                                    name=\"imei\" formControlName=\"imei\">\n                                <ul class=\"help-block\">\n                                    <li *ngIf=\"form.controls.imei.errors?.required && form.controls.imei.dirty && form.controls.imei.touched\">This\n                                        field is required</li>\n                                    <li *ngIf=\"form.controls.imei.errors?.minlength && form.controls.imei.dirty || form.controls.imei.errors?.maxlength && form.controls.imei.dirty\">\n                                     Only 15 Digits Allowed</li>\n                                    <li *ngIf=\"form.controls.imei.errors?.validateIMEI && form.controls.imei.dirty\">IMEI\n                                        must not have any special characters </li>\n                                </ul>\n                            </div>\n                        </div> -->\n      \n                        <div class=\"form-group\">\n                            <div [ngClass]=\"{'has-error': (form.controls.sim.errors && form.controls.sim.dirty) || (!simValid && form.controls.sim.dirty), 'has-success': !form.controls.sim.errors && simValid}\">\n                                <label for=\"\">SIM NUMBER:</label>\n                                <input [(ngModel)]=\"oldsim\" type=\"text\" class=\"form-control input-underline input-lg\" placeholder=\"SIM : 03001234567\" name=\"sim\"\n                                    formControlName=\"sim\">\n                                    <ul class=\"help-block\">\n                                        <li *ngIf=\"form.controls.sim.errors?.required && form.controls.sim.dirty && form.controls.sim.touched\">This\n                                            field is required</li>\n                                        <li *ngIf=\"form.controls.sim.errors?.minlength && form.controls.sim.dirty || form.controls.sim.errors?.maxlength && form.controls.sim.dirty\">\n                                         Only 11 Digits Allowed</li>\n                                        <li *ngIf=\"form.controls.sim.errors?.validateSIM && form.controls.sim.dirty\">SIM\n                                            must not have any special characters </li>\n                                    </ul>\n                            </div>\n                        </div>\n\n                        <div class=\"form-group\">\n                            <div [ngClass]=\"{'has-error': (form.controls.tid.errors && form.controls.tid.dirty) || (!tidValid && form.controls.tid.dirty), 'has-success': !form.controls.tid.errors && tidValid}\">\n                                <label for=\"\">TID:</label>\n                                <input [(ngModel)]=\"oldtid\" type=\"text\" class=\"form-control input-underline input-lg\" placeholder=\"TID : 03001234567\" name=\"tid\"\n                                    formControlName=\"tid\">\n                                    <ul class=\"help-block\">\n                                        <li *ngIf=\"form.controls.tid.errors?.required && form.controls.tid.dirty && form.controls.tid.touched\">This\n                                            field is required</li>\n                                        <li *ngIf=\"form.controls.tid.errors?.minlength && form.controls.tid.dirty || form.controls.tid.errors?.maxlength && form.controls.tid.dirty\">\n                                         Only 16 Digits Allowed</li>\n                                        <li *ngIf=\"form.controls.tid.errors?.validateTID && form.controls.tid.dirty\">TID\n                                            must not have any special characters </li>\n                                    </ul>\n                            </div>\n                        </div>\n\n                        <div class=\"form-group\">\n                                <div [ngClass]=\"{'has-error': (form.controls.sid.errors && form.controls.sid.dirty) || (!sidValid && form.controls.sid.dirty), 'has-success': !form.controls.sid.errors && sidValid}\">\n                                        <label for=\"\">Store ID:</label>\n                                    <input [(ngModel)]=\"oldsid\" type=\"text\" class=\"form-control input-underline input-lg\" placeholder=\"Store ID : 1234567\"\n                                        name=\"sid\" formControlName=\"sid\" minlength=\"7\" maxlength=\"7\">\n                                    <ul class=\"help-block text-white\">\n                                        <li *ngIf=\"form.controls.sid.errors?.required && form.controls.sid.dirty && form.controls.sid.touched\">This\n                                            field is required</li>\n                                        <li *ngIf=\"form.controls.sid.errors?.minlength && form.controls.sid.dirty || form.controls.sid.errors?.maxlength && form.controls.sid.dirty\">\n                                            Only 7 Digits Allowed</li>\n                                        <li *ngIf=\"form.controls.sid.errors?.validateSID && form.controls.sid.dirty\">Store ID\n                                            must not have any special characters </li>\n                                    </ul>\n                                </div>\n                            </div>\n      \n                        <div class=\"form-group\">\n                            <div [ngClass]=\"{'has-error': (form.controls.pan.errors && form.controls.pan.dirty) || (!panValid && form.controls.pan.dirty), 'has-success': !form.controls.pan.errors && panValid}\">\n                                <label for=\"\">PAN:</label>\n                                <input [(ngModel)]=\"oldpan\" type=\"text\" class=\"form-control input-underline input-lg\" placeholder=\"PAN: 012345678911234\" name=\"pan\"\n                                    formControlName=\"pan\" minlength=\"15\" maxlength=\"15\">\n                                    <ul class=\"help-block\">\n                                        <li *ngIf=\"form.controls.pan.errors?.required && form.controls.pan.dirty && form.controls.pan.touched\">This\n                                            field is required</li>\n                                        <li *ngIf=\"form.controls.pan.errors?.minlength && form.controls.pan.dirty || form.controls.pan.errors?.maxlength && form.controls.pan.dirty\">\n                                         Only 15 Digits Allowed</li>\n                                        <li *ngIf=\"form.controls.pan.errors?.validatePAN && form.controls.pan.dirty\">PAN\n                                            must not have any special characters </li>\n                                    </ul>\n                            </div>\n                        </div>\n<!-- \n                        <div class=\"form-group\">\n                            <div [ngClass]=\"{'has-error': (form.controls.pan2.errors && form.controls.pan2.dirty) || (!pan2Valid && form.controls.pan2.dirty), 'has-success': !form.controls.pan2.errors && pan2Valid}\">\n                                <label for=\"\">PAN NUMBER 2:</label>\n                                <input [(ngModel)]=\"oldpan2\" type=\"text\" class=\"form-control input-underline input-lg\" placeholder=\"PAN(04) : 012345678911234\" name=\"pan2\"\n                                    formControlName=\"pan2\" minlength=\"15\" maxlength=\"15\">\n                                    <ul class=\"help-block\">\n                                        <li *ngIf=\"form.controls.pan2.errors?.required && form.controls.pan2.dirty && form.controls.pan2.touched\">This\n                                            field is required</li>\n                                        <li *ngIf=\"form.controls.pan2.errors?.minlength && form.controls.pan2.dirty || form.controls.pan2.errors?.maxlength && form.controls.pan2.dirty\">\n                                         Only 15 Digits Allowed</li>\n                                        <li *ngIf=\"form.controls.pan2.errors?.validatePAN && form.controls.pan2.dirty\">PAN\n                                            must not have any special characters </li>\n                                    </ul>\n                            </div>\n                        </div> -->\n\n                        <div class=\"form-group\">\n                            <div [ngClass]=\"{'has-error': (form.controls.merchant.errors && form.controls.merchant.dirty) || (!merchantValid && form.controls.merchant.dirty), 'has-success': !form.controls.merchant.errors && merchantValid}\">\n                                <label for=\"\">MERCHANT NAME:</label>\n                                <input [(ngModel)]=\"oldmerchant\" type=\"text\" class=\"form-control input-underline input-lg\" placeholder=\"Merchant Name: abcxyz\" name=\"merchant\"\n                                    formControlName=\"merchant\">\n                                    <ul class=\"help-block\">\n                                        <li *ngIf=\"form.controls.merchant.errors?.required && form.controls.merchant.dirty && form.controls.merchant.touched\">This\n                                            field is required</li>\n                                        <li *ngIf=\"form.controls.merchant.errors?.minlength && form.controls.merchant.dirty || form.controls.merchant.errors?.maxlength && form.controls.merchant.dirty\">\n                                                    Not more than 25 Characters Allowed</li>\n                                        <!-- <li *ngIf=\"form.controls.merchant.errors?.validateMerchant && form.controls.merchant.dirty\">Merchant Name\n                                            must not have any special characters </li> -->\n                                    </ul>\n                            </div>\n                        </div>\n                </form>\n    </div>\n    <div class=\"modal-footer\">\n        <button type=\"button\" class=\"btn btn-outline-dark\" (click)=\"c('Close click') || closed()\">Cancel</button>\n        <button type=\"button\" class=\"btn btn-outline-success\" (click)=\"UpdateDevice()\">Save</button>\n        <!-- [disabled]=\"!form.valid\" -->\n    </div>\n</ng-template>"

/***/ }),

/***/ "./src/app/layout/devices/devices.component.scss":
/*!*******************************************************!*\
  !*** ./src/app/layout/devices/devices.component.scss ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".table {\n  background-color: #000000de;\n  color: white;\n  font-size: 14px; }\n\n.filterdiv {\n  margin-right: -15px;\n  float: right; }\n\n.filterdiv input {\n    border: 0;\n    border-bottom: 2px solid #e41a06;\n    background-color: #212121;\n    color: white; }\n\n.my-pagination /deep/ .ngx-pagination {\n  margin-top: 20px;\n  background: #575757 !important; }\n"

/***/ }),

/***/ "./src/app/layout/devices/devices.component.ts":
/*!*****************************************************!*\
  !*** ./src/app/layout/devices/devices.component.ts ***!
  \*****************************************************/
/*! exports provided: DevicesComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DevicesComponent", function() { return DevicesComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/index.js");
/* harmony import */ var _shared_services_bank_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../shared/services/bank.service */ "./src/app/shared/services/bank.service.ts");
/* harmony import */ var angular2_flash_messages__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! angular2-flash-messages */ "./node_modules/angular2-flash-messages/module/index.js");
/* harmony import */ var angular2_flash_messages__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(angular2_flash_messages__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







// import { FilterPipe } from '../../shared/pipes/filter.pipe';
var DevicesComponent = /** @class */ (function () {
    function DevicesComponent(formBuilder, modalService, route, service, router, flashMessages) {
        var _this = this;
        this.formBuilder = formBuilder;
        this.modalService = modalService;
        this.route = route;
        this.service = service;
        this.router = router;
        this.flashMessages = flashMessages;
        this.processing = false;
        this.config = {
            currentPage: 1,
            itemsPerPage: 5
        };
        this.route.queryParamMap
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["map"])(function (params) { return params.get('page'); }))
            .subscribe(function (page) { return _this.config.currentPage = page; });
    }
    // devices = [
    //   {imei: "358472042445412", pan: "1234567980", merchant:"8888-8888-8888-8888", sim:"0900-7601000"},
    //   {imei: "358473042335412", pan: "1234567980", merchant:"9999-9999-9999-9999", sim:"0900-8956200"},
    //   {imei: "358972042445412", pan: "1234567980", merchant:"8888-8888-8888-8888", sim:"0900-7601550"},
    //   {imei: "456815612315412", pan: "1234567980", merchant:"7777-7777-7777-7777", sim:"0900-7689100"}
    // ];
    // disabledev(){
    //   if(this.processing == true){
    //     return 'table-dark'
    //   }else{
    //     this.processing = false;
    //   }
    //   // return 'table-dark'
    // }
    DevicesComponent.prototype.pageChange = function (newPage) {
        this.router.navigate(['devices/'], { queryParams: { page: newPage } });
    };
    DevicesComponent.prototype.open = function (content) {
        var _this = this;
        this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then(function (result) {
            _this.closeResult = "Closed with: " + result;
        }, function (reason) {
            _this.closeResult = "Dismissed " + _this.getDismissReason(reason);
        });
    };
    DevicesComponent.prototype.getDismissReason = function (reason) {
        if (reason === _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_2__["ModalDismissReasons"].ESC) {
            return 'by pressing ESC';
        }
        else if (reason === _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_2__["ModalDismissReasons"].BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        }
        else {
            return "with: " + reason;
        }
    };
    // Disable Device Function
    DevicesComponent.prototype.disablebutton = function (device) {
        var _this = this;
        if (confirm("Are you sure you want to Disable?")) {
            // const token = this.service.getToken();
            var res = {
                imei: device.imei
            };
            console.log(res);
            this.service.BlockDevice(res).subscribe(function (data) {
                _this.flashMessages.show('Device Disable Successfully!', { cssClass: 'alert-danger', timeout: 3000 });
                console.log(data);
            });
            setTimeout(function () {
                _this.router.navigateByUrl('disable-devices');
                setTimeout(function () {
                    _this.router.navigateByUrl('devices');
                }, 100);
            }, 1000);
        }
    };
    DevicesComponent.prototype.ngOnInit = function () {
        this.getAlllist();
        this.createForm();
    };
    DevicesComponent.prototype.closed = function () {
        this.form.reset();
        this.oldpan2 = null;
        // window.location.reload();
    };
    //*********** Reactive Custom Form **************
    DevicesComponent.prototype.createForm = function () {
        this.form = this.formBuilder.group({
            // SIM Input
            sim: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].compose([
                    _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required,
                    _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(11),
                    _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].maxLength(11),
                    this.validateSIM // Custom validation
                ])],
            tid: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].compose([
                    _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required,
                    _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(16),
                    _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].maxLength(16),
                    this.validateTID // Custom validation
                ])],
            sid: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].compose([
                    _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required,
                    _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(7),
                    _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].maxLength(7),
                    this.validateTID // Custom validation
                ])],
            // PAN Input
            pan: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].compose([
                    _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required,
                    _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(15),
                    _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].maxLength(15),
                    this.validatePAN // Custom validation
                ])],
            // pan2: ['', Validators.compose([
            //   Validators.required, // Field is required
            //   Validators.minLength(15), // Minimum length is 15 characters
            //   Validators.maxLength(15), // Maximum length is 15 characters
            //   this.validatePAN // Custom validation
            // ])],
            // Merchant Name Input
            merchant: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].compose([
                    _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required,
                    _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(3),
                    _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].maxLength(25)
                    // this.validateMerchant // Custom validation
                ])],
        });
    };
    // Function to validate SIM no is proper format
    DevicesComponent.prototype.validateSIM = function (controls) {
        var regExp = new RegExp(/^[0-9]{11,11}|[,]$/);
        if (regExp.test(controls.value)) {
            return null;
        }
        else {
            return { 'validateSIM': true };
        }
    };
    // Function to validate PAN no is proper format
    DevicesComponent.prototype.validatePAN = function (controls) {
        var regExp = new RegExp(/^[0-9]{6,6}|[,]$/);
        if (regExp.test(controls.value)) {
            return null;
        }
        else {
            return { 'validatePAN': true };
        }
    };
    DevicesComponent.prototype.validateTID = function (controls) {
        var regExp = new RegExp(/^[0-9]{16,16}|[,]$/);
        if (regExp.test(controls.value)) {
            return null;
        }
        else {
            return { 'validateTID': true };
        }
    };
    DevicesComponent.prototype.validateSID = function (controls) {
        var regExp = new RegExp(/^[0-9]{7,7}|[,]$/);
        if (regExp.test(controls.value)) {
            return null;
        }
        else {
            return { 'validateSID': true };
        }
    };
    // validateMerchant(controls) {
    //   const regExp = new RegExp(/^[a-zA-Z][a-zA-Z ]+$/);
    //   if (regExp.test(controls.value)) {
    //     return null;
    //   } else {
    //     return { 'validateMerchant': true };
    //   }
    // }
    DevicesComponent.prototype.GetDevice = function (device) {
        this.oldimei = device.imei;
        this.oldsim = device.sim;
        this.oldmerchant = device.mn;
        this.oldtid = device.tid;
        this.oldsid = device.storeid;
        this.oldpan = device.pan[2].pan;
        // this.oldpan2 = device.pan[4].pan;
        // if(this.oldpan == null){
        //   this.oldpan = '';
        // }
        // else{
        //   this.oldpan = device.pan[2].pan;
        // }
        // if(this.oldpan2 == null){
        //   this.oldpan2 = '';
        // }else{
        //   this.oldpan2 = device.pan[4].pan;
        // }
        // this.deviceimei = device.imei;
        // console.log(this.devices);
        // console.log(this.oldpan);                                                             
        // console.log(this.oldpan2);
    };
    // Function That Update Device Data
    DevicesComponent.prototype.UpdateDevice = function () {
        var _this = this;
        var deviceobj = {
            imei: this.oldimei,
            sim: this.form.get('sim').value,
            tid: this.form.get('tid').value,
            storeid: this.form.get('sid').value,
            pan: this.form.get('pan').value,
            // pan2: this.form.get('pan2').value,
            mn: this.form.get('merchant').value,
        };
        console.log(deviceobj);
        // const token = this.service.getToken();
        if (confirm('Are you sure want to update ?')) {
            this.service.UpdateDeviceData(deviceobj).subscribe(function (data) {
                if (data.code == "successful") {
                    console.log(data);
                    _this.flashMessages.show('Device Update Successfully!', { cssClass: 'alert-success', timeout: 1500 });
                    _this.form.reset();
                }
                else {
                    alert(data.message);
                    _this.flashMessages.show(data.message, { cssClass: 'alert-danger', timeout: 4000 });
                }
            });
            setTimeout(function () {
                window.location.reload();
            }, 1800);
        }
    };
    DevicesComponent.prototype.getAlllist = function () {
        var _this = this;
        // const token = this.service.getToken();
        this.service.getDevices().subscribe(function (data) {
            _this.devices = data;
            console.log(_this.devices);
        }, function (err) {
            if (err.status == 0 || err.status == 401) {
                _this.flashMessages.show('Session Expired Please Login again', { cssClass: 'alert-danger', timeout: 1000 });
                _this.service.RemoveToken();
                console.log('Connection Time Out');
                console.log('Session Expired');
            }
        });
    };
    DevicesComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-devices',
            template: __webpack_require__(/*! ./devices.component.html */ "./src/app/layout/devices/devices.component.html"),
            styles: [__webpack_require__(/*! ./devices.component.scss */ "./src/app/layout/devices/devices.component.scss")],
        }),
        __metadata("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"],
            _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_2__["NgbModal"],
            _angular_router__WEBPACK_IMPORTED_MODULE_5__["ActivatedRoute"],
            _shared_services_bank_service__WEBPACK_IMPORTED_MODULE_3__["BankService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"],
            angular2_flash_messages__WEBPACK_IMPORTED_MODULE_4__["FlashMessagesService"]])
    ], DevicesComponent);
    return DevicesComponent;
}());



/***/ }),

/***/ "./src/app/layout/devices/devices.module.ts":
/*!**************************************************!*\
  !*** ./src/app/layout/devices/devices.module.ts ***!
  \**************************************************/
/*! exports provided: DevicesModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DevicesModule", function() { return DevicesModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _devices_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./devices-routing.module */ "./src/app/layout/devices/devices-routing.module.ts");
/* harmony import */ var _devices_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./devices.component */ "./src/app/layout/devices/devices.component.ts");
/* harmony import */ var _shared__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../shared */ "./src/app/shared/index.ts");
/* harmony import */ var _my_pipe_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../my-pipe.module */ "./src/app/layout/my-pipe.module.ts");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};








var DevicesModule = /** @class */ (function () {
    function DevicesModule() {
    }
    DevicesModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _devices_routing_module__WEBPACK_IMPORTED_MODULE_3__["DevicesRoutingModule"],
                _shared__WEBPACK_IMPORTED_MODULE_5__["PageHeaderModule"],
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_7__["NgbModule"].forRoot(),
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["ReactiveFormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _my_pipe_module__WEBPACK_IMPORTED_MODULE_6__["MyPipeModule"]
            ],
            declarations: [_devices_component__WEBPACK_IMPORTED_MODULE_4__["DevicesComponent"]]
        })
    ], DevicesModule);
    return DevicesModule;
}());



/***/ })

}]);
//# sourceMappingURL=devices-devices-module.js.map