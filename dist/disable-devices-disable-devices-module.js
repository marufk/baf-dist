(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["disable-devices-disable-devices-module"],{

/***/ "./src/app/layout/disable-devices/disable-devices-routing.module.ts":
/*!**************************************************************************!*\
  !*** ./src/app/layout/disable-devices/disable-devices-routing.module.ts ***!
  \**************************************************************************/
/*! exports provided: DisableDevicesRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DisableDevicesRoutingModule", function() { return DisableDevicesRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _disable_devices_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./disable-devices.component */ "./src/app/layout/disable-devices/disable-devices.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var routes = [
    {
        path: '',
        component: _disable_devices_component__WEBPACK_IMPORTED_MODULE_2__["DisableDevicesComponent"]
    }
];
var DisableDevicesRoutingModule = /** @class */ (function () {
    function DisableDevicesRoutingModule() {
    }
    DisableDevicesRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], DisableDevicesRoutingModule);
    return DisableDevicesRoutingModule;
}());



/***/ }),

/***/ "./src/app/layout/disable-devices/disable-devices.component.html":
/*!***********************************************************************!*\
  !*** ./src/app/layout/disable-devices/disable-devices.component.html ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"form-group col-md-4 filterdiv\">\n    <input class=\"form-control\" [(ngModel)]=\"searchText\" placeholder=\"Filter By IMEI\">\n</div>\n<div class=\"table-responsive\">\n    <table class=\"table table-hover\">\n        <thead>\n            <tr>\n                <th>#</th>\n                <th scope=\"col\">Device IMEI </th>\n                <th scope=\"col\">Device PAN</th>\n                <th scope=\"col\">TID</th>\n                <th scope=\"col\">Store ID</th>\n                <th scope=\"col\">Merchant Name</th>\n                <th scope=\"col\">SIM Number</th>\n                <th scope=\"col\">Action </th>\n            </tr>\n        </thead>\n        <tbody>\n            <tr *ngFor=\"let device of devices | filter: searchText  | paginate: config let i= index\">\n                <td>{{i+1}}</td>\n                <td>{{device.imei}}</td>\n                <td>{{device.pan[2].pan}}</td>\n                <!-- <td>\n                    <select class=\"custom-select\">\n                        <option disabled selected>View PAN</option>\n                        <option value=\"1\">{{device.pan[2].pan}}</option>\n                        <option value=\"1\">{{device.pan[4].pan}}</option>\n                    </select>\n                </td> -->\n                <td>{{device.tid}}</td>\n                <td>{{device.storeid}}</td>\n                <td>{{device.mn}}</td>\n                <td>{{device.sim}}</td>\n                <td>\n                    <button class=\"btn btn-outline-success\" (click)=\"enablebutton(device)\">Enable</button>&nbsp;&nbsp;\n                    <button class=\"btn btn-outline-danger\" (click)=\"deletebutton(device)\">Delete</button>\n                </td>\n            </tr>\n        </tbody>\n    </table>\n</div>\n<pagination-controls *ngIf=\"devices\" class=\"my-pagination\" (pageChange)=\"pageChange($event)\"></pagination-controls>\n"

/***/ }),

/***/ "./src/app/layout/disable-devices/disable-devices.component.scss":
/*!***********************************************************************!*\
  !*** ./src/app/layout/disable-devices/disable-devices.component.scss ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".table {\n  background-color: #000000de;\n  color: white;\n  font-size: 14px; }\n\n.filterdiv {\n  margin-right: -15px;\n  float: right; }\n\n.filterdiv input {\n    border: 0;\n    border-bottom: 2px solid #e41a06;\n    background-color: #212121;\n    color: white; }\n\n.my-pagination /deep/ .ngx-pagination {\n  margin-top: 20px;\n  background: #575757 !important; }\n"

/***/ }),

/***/ "./src/app/layout/disable-devices/disable-devices.component.ts":
/*!*********************************************************************!*\
  !*** ./src/app/layout/disable-devices/disable-devices.component.ts ***!
  \*********************************************************************/
/*! exports provided: DisableDevicesComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DisableDevicesComponent", function() { return DisableDevicesComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _shared_services_bank_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../shared/services/bank.service */ "./src/app/shared/services/bank.service.ts");
/* harmony import */ var angular2_flash_messages__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! angular2-flash-messages */ "./node_modules/angular2-flash-messages/module/index.js");
/* harmony import */ var angular2_flash_messages__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(angular2_flash_messages__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var DisableDevicesComponent = /** @class */ (function () {
    function DisableDevicesComponent(service, route, router, flashMessages) {
        var _this = this;
        this.service = service;
        this.route = route;
        this.router = router;
        this.flashMessages = flashMessages;
        this.config = {
            currentPage: 1,
            itemsPerPage: 5
        };
        this.route.queryParamMap
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (params) { return params.get('page'); }))
            .subscribe(function (page) { return _this.config.currentPage = page; });
    }
    DisableDevicesComponent.prototype.pageChange = function (newPage) {
        this.router.navigate(['disable-devices/'], { queryParams: { page: newPage } });
    };
    DisableDevicesComponent.prototype.ngOnInit = function () {
        this.getAlllist();
    };
    DisableDevicesComponent.prototype.getAlllist = function () {
        var _this = this;
        // const token = this.service.getToken();
        this.service.getDisableDevices().subscribe(function (data) {
            _this.devices = data;
            console.log(_this.devices);
        }, function (err) {
            if (err.status == 0 || err.status == 401) {
                _this.flashMessages.show('Session Expired Please Login again', { cssClass: 'alert-danger', timeout: 3000 });
                _this.service.RemoveToken();
                console.log('Connection Time Out');
                console.log('Session Expired');
            }
        });
    };
    DisableDevicesComponent.prototype.enablebutton = function (device) {
        var _this = this;
        if (confirm("Are you sure you want to Enable?")) {
            // const token = this.service.getToken();
            var res = {
                imei: device.imei
            };
            console.log(res);
            this.service.UnBlockDevice(res).subscribe(function (data) {
                console.log(data);
                _this.flashMessages.show('Device Enable Successfully!', { cssClass: 'alert-success', timeout: 1000 });
            });
            setTimeout(function () {
                _this.router.navigateByUrl('devices');
                setTimeout(function () {
                    _this.router.navigateByUrl('disable-devices');
                }, 100);
            }, 1000);
        }
    };
    DisableDevicesComponent.prototype.deletebutton = function (device) {
        var res = {
            imei: device.imei
        };
        if (confirm('Are you sure you want to delete ?')) {
            // const token = this.service.getToken();
            this.service.DeleteDevice(res).subscribe(function (data) {
                console.log(data);
            });
            setTimeout(function () {
                window.location.reload();
            }, 1000);
        }
    };
    DisableDevicesComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-disable-devices',
            template: __webpack_require__(/*! ./disable-devices.component.html */ "./src/app/layout/disable-devices/disable-devices.component.html"),
            styles: [__webpack_require__(/*! ./disable-devices.component.scss */ "./src/app/layout/disable-devices/disable-devices.component.scss")]
        }),
        __metadata("design:paramtypes", [_shared_services_bank_service__WEBPACK_IMPORTED_MODULE_1__["BankService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
            angular2_flash_messages__WEBPACK_IMPORTED_MODULE_2__["FlashMessagesService"]])
    ], DisableDevicesComponent);
    return DisableDevicesComponent;
}());



/***/ }),

/***/ "./src/app/layout/disable-devices/disable-devices.module.ts":
/*!******************************************************************!*\
  !*** ./src/app/layout/disable-devices/disable-devices.module.ts ***!
  \******************************************************************/
/*! exports provided: DisableDevicesModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DisableDevicesModule", function() { return DisableDevicesModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _disable_devices_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./disable-devices-routing.module */ "./src/app/layout/disable-devices/disable-devices-routing.module.ts");
/* harmony import */ var _disable_devices_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./disable-devices.component */ "./src/app/layout/disable-devices/disable-devices.component.ts");
/* harmony import */ var _shared__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../shared */ "./src/app/shared/index.ts");
/* harmony import */ var _my_pipe_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../my-pipe.module */ "./src/app/layout/my-pipe.module.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







var DisableDevicesModule = /** @class */ (function () {
    function DisableDevicesModule() {
    }
    DisableDevicesModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _disable_devices_routing_module__WEBPACK_IMPORTED_MODULE_3__["DisableDevicesRoutingModule"],
                _shared__WEBPACK_IMPORTED_MODULE_5__["PageHeaderModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _my_pipe_module__WEBPACK_IMPORTED_MODULE_6__["MyPipeModule"]
            ],
            declarations: [_disable_devices_component__WEBPACK_IMPORTED_MODULE_4__["DisableDevicesComponent"]]
        })
    ], DisableDevicesModule);
    return DisableDevicesModule;
}());



/***/ })

}]);
//# sourceMappingURL=disable-devices-disable-devices-module.js.map