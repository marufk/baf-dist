(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["users-users-module"],{

/***/ "./src/app/layout/users/users-routing.module.ts":
/*!******************************************************!*\
  !*** ./src/app/layout/users/users-routing.module.ts ***!
  \******************************************************/
/*! exports provided: UsersRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UsersRoutingModule", function() { return UsersRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _users_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./users.component */ "./src/app/layout/users/users.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var routes = [
    {
        path: '',
        component: _users_component__WEBPACK_IMPORTED_MODULE_2__["UsersComponent"]
    }
];
var UsersRoutingModule = /** @class */ (function () {
    function UsersRoutingModule() {
    }
    UsersRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], UsersRoutingModule);
    return UsersRoutingModule;
}());



/***/ }),

/***/ "./src/app/layout/users/users.component.html":
/*!***************************************************!*\
  !*** ./src/app/layout/users/users.component.html ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n<div class=\"row\">\n<div class=\"form-group col-md-4 filterdiv\">\n    <input class=\"form-control\" [(ngModel)]=\"searchText\" placeholder=\"Filter By Email\">\n</div>\n</div>\n<div [@routerTransition]>\n    <!-- <app-page-header [heading]=\"'Response Request'\" [icon]=\"'fa-bar-chart-o'\"></app-page-header> -->\n    <ngb-accordion #acc=\"ngbAccordion\" activeIds=\"\" *ngFor=\"let user of users | emailfilter:searchText | paginate: config\">\n        <ngb-panel *ngIf=\"user[1] !== 'marufk@gmail.com'\" title=\"Email: {{user[1]}}\">\n            <ng-template ngbPanelContent>\n                <div>\n                    <span><strong>Name: </strong> <span>{{user[0]}}</span></span>\n                </div>\n                <hr>\n                <div style=\"text-align:center\">\n                    <button type=\"button\" class=\"btn btn-outline-primary\" (click)=\"open(content) || getEmail(user[1])\">Change Password</button>&nbsp;&nbsp;\n                    <button type=\"button\" class=\"btn btn-outline-danger\" (click)=\"BlockUser(user[1])\">Block User</button>\n                </div>\n            </ng-template>\n        </ngb-panel>\n    </ngb-accordion>\n\n    <ng-template #content let-modal let-c=\"close\" let-d=\"dismiss\">\n        <div class=\"modal-header\">\n            <h4 class=\"modal-title\" id=\"modal-basic-title\">Change Password</h4>\n            <button type=\"button\" class=\"close\" aria-label=\"Close\" (click)=\"d('Cross click')\">\n                <span aria-hidden=\"true\">&times;</span>\n            </button>\n        </div>\n        <div class=\"modal-body\">\n            <form [formGroup]=\"form\">\n                <div class=\"form-group\">\n                    <div [ngClass]=\"{'has-error': (form.controls.password.errors && form.controls.password.dirty), 'has-success': !form.controls.password.errors}\">\n                        <input type=\"password\" class=\"form-control\" id=\"passcontrol\" placeholder=\"New Password\" name=\"password\" passwordToggle\n                            formControlName=\"password\">\n                        <ul class=\"help-block\">\n                            <li *ngIf=\"form.controls.password.errors?.required && form.controls.password.dirty\">This\n                                field is required</li>\n                            <li *ngIf=\"form.controls.password.errors?.minlength && form.controls.password.dirty || form.controls.password.errors?.maxlength && form.controls.password.dirty \">Minimum\n                                characters: 6, Maximum characters: 16</li>\n                            <li *ngIf=\"form.controls.password.errors?.validatePassword && form.controls.password.dirty\">Password\n                                must be at least 6 characters but not more than 16</li>\n                        </ul>\n                    </div>\n                </div>\n                <div class=\"form-group\">\n                    <div [ngClass]=\"{'has-error': (form.controls.confirmpass.errors && form.controls.confirmpass.dirty) || (form.errors?.matchingPasswords && form.controls.confirmpass.dirty), 'has-success': !form.controls.confirmpass.errors && !form.errors?.matchingPasswords}\">\n                        <input type=\"password\" class=\"form-control\" id=\"cpasscontrol\" placeholder=\"Confirm Password\"\n                            name=\"confirmpass\" passwordToggle formControlName=\"confirmpass\">\n                        <ul class=\"help-block\">\n                            <li *ngIf=\"form.controls.confirmpass.errors?.required && form.controls.confirmpass.dirty\">This\n                                field is required</li>\n                            <li *ngIf=\"form.errors?.matchingPasswords && form.controls.confirmpass.dirty\">Password\n                                do not match</li>\n                        </ul>\n                    </div>\n                </div>\n            </form>\n        </div>\n        <div class=\"modal-footer\">\n            <button type=\"button\" class=\"btn btn-outline-dark\" (click)=\"c('Close click') || closed()\">Cancel</button>\n            <button type=\"button\" class=\"btn btn-outline-success\" [disabled]=\"!form.valid\" (click)=\"PasswordChanged()\">Save</button>\n        </div>\n    </ng-template>\n    <pagination-controls *ngIf=\"users\" class=\"my-pagination\" (pageChange)=\"pageChange($event)\"></pagination-controls>\n</div>\n"

/***/ }),

/***/ "./src/app/layout/users/users.component.scss":
/*!***************************************************!*\
  !*** ./src/app/layout/users/users.component.scss ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "::ng-deep div.card-header:hover {\n  background-color: #575757; }\n\n::ng-deep div.card-header .btn-link {\n  color: white;\n  text-decoration: none !important; }\n\n::ng-deep div.card-header {\n  border: 2px solid white;\n  background-color: #000000de; }\n\n::ng-deep div.card-body {\n  background-color: #000000de;\n  color: white; }\n\n.filterdiv {\n  margin-right: -15px;\n  float: right; }\n\n.filterdiv input {\n    border: 0;\n    border-bottom: 2px solid #e41a06;\n    background-color: #212121;\n    color: white; }\n\n.my-pagination /deep/ .ngx-pagination {\n  margin-top: 20px;\n  background: #575757 !important; }\n"

/***/ }),

/***/ "./src/app/layout/users/users.component.ts":
/*!*************************************************!*\
  !*** ./src/app/layout/users/users.component.ts ***!
  \*************************************************/
/*! exports provided: UsersComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UsersComponent", function() { return UsersComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _router_animations__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../router.animations */ "./src/app/router.animations.ts");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/index.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _shared_services_bank_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../shared/services/bank.service */ "./src/app/shared/services/bank.service.ts");
/* harmony import */ var angular2_flash_messages__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! angular2-flash-messages */ "./node_modules/angular2-flash-messages/module/index.js");
/* harmony import */ var angular2_flash_messages__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(angular2_flash_messages__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var UsersComponent = /** @class */ (function () {
    // usersres = [
    //   { email: "maruf@gmail.com", name: "Maruf Khan" },
    //   { email: "adnan@gmail.com", name: "Adnan" },
    //   { email: "khurramk@gmail.com", name: "KhurramKalimi" }
    // ]
    function UsersComponent(modalService, formBuilder, router, route, flashMessages, service) {
        var _this = this;
        this.modalService = modalService;
        this.formBuilder = formBuilder;
        this.router = router;
        this.route = route;
        this.flashMessages = flashMessages;
        this.service = service;
        this.createForm();
        this.config = {
            currentPage: 1,
            itemsPerPage: 10
        };
        this.route.queryParamMap
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["map"])(function (params) { return params.get('page'); }))
            .subscribe(function (page) { return _this.config.currentPage = page; });
    }
    UsersComponent.prototype.pageChange = function (newPage) {
        this.router.navigate(['users/'], { queryParams: { page: newPage } });
    };
    UsersComponent.prototype.ngOnInit = function () {
        this.GetApprovedUser();
    };
    UsersComponent.prototype.createForm = function () {
        this.form = this.formBuilder.group({
            // Password Input
            password: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].compose([
                    _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required,
                    _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].minLength(6),
                    _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].maxLength(16),
                ])],
            // Confirm Password Input
            confirmpass: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required] // Field is required
        }, {
            validator: this.matchingPasswords('password', 'confirmpass')
        });
    };
    UsersComponent.prototype.closed = function () {
        this.form.reset();
    };
    // Funciton to ensure passwords match
    UsersComponent.prototype.matchingPasswords = function (password, confirmpass) {
        return function (group) {
            // Check if both fields are the same
            if (group.controls[password].value === group.controls[confirmpass].value) {
                return null; // Return as a match
            }
            else {
                return { 'matchingPasswords': true }; // Return as error: do not match
            }
        };
    };
    UsersComponent.prototype.open = function (content) {
        var _this = this;
        this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then(function (result) {
            _this.closeResult = "Closed with: " + result;
        }, function (reason) {
            _this.closeResult = "Dismissed " + _this.getDismissReason(reason);
        });
    };
    UsersComponent.prototype.getDismissReason = function (reason) {
        if (reason === _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_2__["ModalDismissReasons"].ESC) {
            return 'by pressing ESC';
        }
        else if (reason === _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_2__["ModalDismissReasons"].BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        }
        else {
            return "with: " + reason;
        }
    };
    UsersComponent.prototype.getEmail = function (email) {
        this.useremail = email;
    };
    // Change Password Function
    UsersComponent.prototype.PasswordChanged = function () {
        var _this = this;
        var res = {
            email: this.useremail,
            password: this.form.get('password').value
        };
        if (confirm("Are you sure you want to Change Password ?")) {
            // const token = this.service.getToken();
            this.service.ChangePassword(res).subscribe(function (data) {
                _this.form.reset();
                console.log(data);
                alert("Password Changed Successfully");
                _this.flashMessages.show('Password Changed Successfully!', { cssClass: 'alert-success', timeout: 2000 });
            });
            setTimeout(function () {
                window.location.reload();
            }, 2000);
        }
    };
    UsersComponent.prototype.BlockUser = function (useremail) {
        var _this = this;
        var res = {
            enable: false,
            email: useremail,
        };
        if (confirm("Are you sure you want to block User ?")) {
            // const token = this.service.getToken();
            this.service.BlockedUser(res).subscribe(function (data) {
                _this.flashMessages.show('User Blocked!', { cssClass: 'alert-danger', timeout: 3000 });
            });
            this.router.navigateByUrl('user-pending-request');
            setTimeout(function () {
                _this.router.navigateByUrl('users');
            }, 100);
        }
    };
    UsersComponent.prototype.GetApprovedUser = function () {
        var _this = this;
        // const token = this.service.getToken();
        this.service.getApprovedUser().subscribe(function (data) {
            _this.users = data;
        }, function (err) {
            if (err.status == 0 || err.status == 401) {
                _this.flashMessages.show('Session Expired Please Login again', { cssClass: 'alert-danger', timeout: 3000 });
                _this.service.RemoveToken();
                console.log('Session Expired');
            }
        });
    };
    UsersComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-users',
            template: __webpack_require__(/*! ./users.component.html */ "./src/app/layout/users/users.component.html"),
            styles: [__webpack_require__(/*! ./users.component.scss */ "./src/app/layout/users/users.component.scss")],
            animations: [Object(_router_animations__WEBPACK_IMPORTED_MODULE_1__["routerTransition"])()]
        }),
        __metadata("design:paramtypes", [_ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_2__["NgbModal"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"],
            _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_6__["ActivatedRoute"],
            angular2_flash_messages__WEBPACK_IMPORTED_MODULE_5__["FlashMessagesService"],
            _shared_services_bank_service__WEBPACK_IMPORTED_MODULE_4__["BankService"]])
    ], UsersComponent);
    return UsersComponent;
}());



/***/ }),

/***/ "./src/app/layout/users/users.module.ts":
/*!**********************************************!*\
  !*** ./src/app/layout/users/users.module.ts ***!
  \**********************************************/
/*! exports provided: UsersModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UsersModule", function() { return UsersModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/index.js");
/* harmony import */ var _users_routing_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./users-routing.module */ "./src/app/layout/users/users-routing.module.ts");
/* harmony import */ var _users_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./users.component */ "./src/app/layout/users/users.component.ts");
/* harmony import */ var _my_pipe_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../my-pipe.module */ "./src/app/layout/my-pipe.module.ts");
/* harmony import */ var ngx_password_toggle__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ngx-password-toggle */ "./node_modules/ngx-password-toggle/ngx-password-toggle.umd.js");
/* harmony import */ var ngx_password_toggle__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(ngx_password_toggle__WEBPACK_IMPORTED_MODULE_7__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







// import { EmailfilterPipe } from '../../shared/pipes/emailfilter.pipe';


var UsersModule = /** @class */ (function () {
    function UsersModule() {
    }
    UsersModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _users_routing_module__WEBPACK_IMPORTED_MODULE_4__["UsersRoutingModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["ReactiveFormsModule"],
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__["NgbModule"].forRoot(),
                ngx_password_toggle__WEBPACK_IMPORTED_MODULE_7__["NgxPasswordToggleModule"],
                _my_pipe_module__WEBPACK_IMPORTED_MODULE_6__["MyPipeModule"]
            ],
            declarations: [_users_component__WEBPACK_IMPORTED_MODULE_5__["UsersComponent"]]
        })
    ], UsersModule);
    return UsersModule;
}());



/***/ })

}]);
//# sourceMappingURL=users-users-module.js.map