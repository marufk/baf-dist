(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["push-notification-push-notification-module"],{

/***/ "./src/app/layout/push-notification/push-notification-routing.module.ts":
/*!******************************************************************************!*\
  !*** ./src/app/layout/push-notification/push-notification-routing.module.ts ***!
  \******************************************************************************/
/*! exports provided: PushNotificationRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PushNotificationRoutingModule", function() { return PushNotificationRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _push_notification_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./push-notification.component */ "./src/app/layout/push-notification/push-notification.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var routes = [
    {
        path: '',
        component: _push_notification_component__WEBPACK_IMPORTED_MODULE_2__["PushNotificationComponent"]
    }
];
var PushNotificationRoutingModule = /** @class */ (function () {
    function PushNotificationRoutingModule() {
    }
    PushNotificationRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], PushNotificationRoutingModule);
    return PushNotificationRoutingModule;
}());



/***/ }),

/***/ "./src/app/layout/push-notification/push-notification.component.html":
/*!***************************************************************************!*\
  !*** ./src/app/layout/push-notification/push-notification.component.html ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"login-page\" [@routerTransition]>\n    <div class=\"row justify-content-md-center\">\n        <div class=\"col-md-4 crytal-black\">\n            <img class=\"user-avatar\" src=\"assets/images/BA-logo.png\" width=\"70px\" />\n            <h1 class=\"card-title\">Push Configuration</h1>\n            <form method=\"post\" role=\"form\" [formGroup]=\"form\" (submit)=\"onPushNotify()\">\n                <div class=\"form-content\">\n\n                    <!-- <div class=\"form-group\">\n                        <div [ngClass]=\"{'has-error': (form.controls.imei.errors && form.controls.imei.dirty), 'has-success': !form.controls.imei.errors && imeiValid}\">\n                            <input type=\"text\" class=\"form-control input-underline input-lg\" placeholder=\"IMEI : 358472042445412\"\n                                name=\"imei\" formControlName=\"imei\">\n                            <ul class=\"help-block text-white\">\n                                <li *ngIf=\"form.controls.imei.errors?.required && form.controls.imei.dirty && form.controls.imei.touched\">This\n                                    field is required</li>\n                                <li *ngIf=\"form.controls.imei.errors?.minlength && form.controls.imei.dirty || form.controls.imei.errors?.maxlength && form.controls.imei.dirty\">\n                                    Only 15 Digits Allowed</li>\n                                <li *ngIf=\"form.controls.imei.errors?.validateIMEI && form.controls.imei.dirty\">IMEI\n                                    must not have any special characters </li>\n                            </ul>\n                        </div>\n                    </div> -->\n                    <div class=\"form-group\">\n                        <select class=\"custom-select\" name=\"imei\" formControlName=\"imei\">\n                            <option value=\"\" disabled selected>Select IMEI</option>\n                            <option value=\"{{ list.imei }}\" *ngFor=\"let list of allimei\">{{list.imei}}</option>\n                        </select>\n                    </div>\n\n                </div>\n                <button class=\"btn rounded-btn\" type=\"reset\"> Clear </button>&nbsp;&nbsp;\n                <button class=\"btn rounded-btn\" type=\"submit\" [disabled]=\"!form.valid\"> Send </button>&nbsp;\n                <!-- <a class=\"btn rounded-btn\" [routerLink]=\"['/login']\"> Log in </a> -->\n            </form>\n        </div>\n    </div>\n</div>\n"

/***/ }),

/***/ "./src/app/layout/push-notification/push-notification.component.scss":
/*!***************************************************************************!*\
  !*** ./src/app/layout/push-notification/push-notification.component.scss ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ":host {\n  display: block; }\n\n.login-page {\n  top: 0;\n  left: 0;\n  right: 0;\n  bottom: 0;\n  text-align: center;\n  color: #fff;\n  padding: 1em; }\n\n.login-page .crytal-black {\n    background: rgba(0, 0, 0, 0.658824);\n    padding: 25px; }\n\n.login-page .crytal-black .bottom {\n      padding-top: 8px;\n      font-size: 14px; }\n\n.login-page .crytal-black .bottom p {\n        margin-bottom: 0rem; }\n\n.login-page .crytal-black .bottom a {\n        color: #ced4da;\n        text-decoration: none; }\n\n.login-page .crytal-black .bottom a:hover {\n          color: #bc2418; }\n\n.login-page .crytal-black .forgot-pass {\n      float: right;\n      margin-top: -55px;\n      font-size: 13px; }\n\n.login-page .crytal-black .forgot-pass a {\n        color: #ced4da;\n        text-decoration: none; }\n\n.login-page .crytal-black .forgot-pass a:hover {\n          color: #bc2418; }\n\n.login-page .col-lg-4 {\n    padding: 0; }\n\n.login-page .input-lg {\n    height: 46px;\n    padding: 10px 16px;\n    font-size: 18px;\n    line-height: 1.3333333;\n    border-radius: 0; }\n\n.login-page .input-underline {\n    background: 0 0;\n    border: none;\n    box-shadow: none;\n    border-bottom: 2px solid rgba(255, 255, 255, 0.5);\n    color: #fff;\n    border-radius: 0; }\n\n.login-page .input-underline:focus {\n    border-bottom: 2px solid #fff;\n    box-shadow: none; }\n\n.login-page .rounded-btn {\n    border-radius: 50px;\n    color: rgba(255, 255, 255, 0.8);\n    background: #222;\n    border: 2px solid rgba(255, 255, 255, 0.8);\n    font-size: 18px;\n    line-height: 40px;\n    padding: 0 25px; }\n\n.login-page .rounded-btn:hover,\n  .login-page .rounded-btn:focus,\n  .login-page .rounded-btn:active,\n  .login-page .rounded-btn:visited {\n    color: white;\n    border: 2px solid white;\n    outline: none; }\n\n.login-page h1 {\n    font-weight: 300;\n    margin-top: 20px;\n    margin-bottom: 10px;\n    font-size: 36px; }\n\n.login-page h1 small {\n      color: rgba(255, 255, 255, 0.7); }\n\n.login-page .form-group {\n    padding: 8px 0; }\n\n.login-page .form-group input::-webkit-input-placeholder {\n      color: rgba(255, 255, 255, 0.6) !important; }\n\n.login-page .form-group input:-moz-placeholder {\n      /* Firefox 18- */\n      color: rgba(255, 255, 255, 0.6) !important; }\n\n.login-page .form-group input::-moz-placeholder {\n      /* Firefox 19+ */\n      color: rgba(255, 255, 255, 0.6) !important; }\n\n.login-page .form-group input:-ms-input-placeholder {\n      color: rgba(255, 255, 255, 0.6) !important; }\n\n.login-page .form-content {\n    padding: 40px 0; }\n\n.login-page .user-avatar {\n    -webkit-border-radius: 50%; }\n\n@media (min-width: 768px) and (max-width: 1200px) {\n    .login-page .col-md-4 {\n      flex: 0 0 42.333333%;\n      max-width: 42.333333%; } }\n\n.input-underline.ng-invalid.ng-touched, select.ng-invalid.ng-touched {\n  border-bottom: 2px solid red; }\n\n.help-block {\n  font-size: 14px;\n  float: left;\n  text-align: left; }\n"

/***/ }),

/***/ "./src/app/layout/push-notification/push-notification.component.ts":
/*!*************************************************************************!*\
  !*** ./src/app/layout/push-notification/push-notification.component.ts ***!
  \*************************************************************************/
/*! exports provided: PushNotificationComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PushNotificationComponent", function() { return PushNotificationComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _router_animations__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../router.animations */ "./src/app/router.animations.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _shared_services_bank_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../shared/services/bank.service */ "./src/app/shared/services/bank.service.ts");
/* harmony import */ var angular2_flash_messages__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! angular2-flash-messages */ "./node_modules/angular2-flash-messages/module/index.js");
/* harmony import */ var angular2_flash_messages__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(angular2_flash_messages__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var PushNotificationComponent = /** @class */ (function () {
    function PushNotificationComponent(formBuilder, flashMessages, service, router) {
        this.formBuilder = formBuilder;
        this.flashMessages = flashMessages;
        this.service = service;
        this.router = router;
        this.createForm();
    }
    PushNotificationComponent.prototype.ngOnInit = function () {
        var _this = this;
        // const token = this.service.getToken();
        this.service.getDevices().subscribe(function (data) {
            _this.allimei = data;
            // console.log(this.allimei);
        }, function (err) {
            if (err.status == 0 || err.status == 401) {
                _this.flashMessages.show('Session Expired Please Login again', { cssClass: 'alert-danger', timeout: 3000 });
                _this.service.RemoveToken();
                console.log('Session Expired');
            }
        });
    };
    PushNotificationComponent.prototype.createForm = function () {
        this.form = this.formBuilder.group({
            // IMEI Input
            // imei: ['', Validators.compose([
            //   Validators.required, // Field is required
            //   Validators.minLength(15), // Minimum length is 15 characters
            //   Validators.maxLength(15), // Maximum length is 15 characters
            //   this.validateIMEI // Custom validation
            // ])],
            imei: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required
                ])]
        });
    };
    // Function to validate IMEI no is proper format
    // validateIMEI(controls) {
    //   const regExp = new RegExp(/^[0-9]{15,15}|[,]$/);
    //   if (regExp.test(controls.value)) {
    //     return null;
    //   } else {
    //     return { 'validateIMEI': true };
    //   }
    // }
    PushNotificationComponent.prototype.onPushNotify = function () {
        var _this = this;
        var obj = {
            // imei: this.form.get('imei').value
            imei: this.form.get('imei').value
        };
        console.log(obj);
        // const token = this.service.getToken();
        this.service.SendPushNotification(obj).subscribe(function (data) {
            if (data.code == "successful") {
                _this.form.reset();
                _this.flashMessages.show('Configuration Send To Device Successfully!', { cssClass: 'alert-success', timeout: 3000 });
            }
            else {
                _this.flashMessages.show(data.message, { cssClass: 'alert-danger', timeout: 3000 });
            }
            console.log(data);
        });
    };
    PushNotificationComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-push-notification',
            template: __webpack_require__(/*! ./push-notification.component.html */ "./src/app/layout/push-notification/push-notification.component.html"),
            styles: [__webpack_require__(/*! ./push-notification.component.scss */ "./src/app/layout/push-notification/push-notification.component.scss")],
            animations: [Object(_router_animations__WEBPACK_IMPORTED_MODULE_1__["routerTransition"])()]
        }),
        __metadata("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"],
            angular2_flash_messages__WEBPACK_IMPORTED_MODULE_4__["FlashMessagesService"],
            _shared_services_bank_service__WEBPACK_IMPORTED_MODULE_3__["BankService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"]])
    ], PushNotificationComponent);
    return PushNotificationComponent;
}());



/***/ }),

/***/ "./src/app/layout/push-notification/push-notification.module.ts":
/*!**********************************************************************!*\
  !*** ./src/app/layout/push-notification/push-notification.module.ts ***!
  \**********************************************************************/
/*! exports provided: PushNotificationModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PushNotificationModule", function() { return PushNotificationModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _shared__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./../../shared */ "./src/app/shared/index.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _push_notification_routing_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./push-notification-routing.module */ "./src/app/layout/push-notification/push-notification-routing.module.ts");
/* harmony import */ var _push_notification_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./push-notification.component */ "./src/app/layout/push-notification/push-notification.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var PushNotificationModule = /** @class */ (function () {
    function PushNotificationModule() {
    }
    PushNotificationModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                _shared__WEBPACK_IMPORTED_MODULE_2__["PageHeaderModule"],
                _push_notification_routing_module__WEBPACK_IMPORTED_MODULE_4__["PushNotificationRoutingModule"]
            ],
            declarations: [_push_notification_component__WEBPACK_IMPORTED_MODULE_5__["PushNotificationComponent"]]
        })
    ], PushNotificationModule);
    return PushNotificationModule;
}());



/***/ })

}]);
//# sourceMappingURL=push-notification-push-notification-module.js.map