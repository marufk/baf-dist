(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["registered-posdevice-registered-posdevice-module"],{

/***/ "./src/app/layout/registered-posdevice/registered-posdevice-routing.module.ts":
/*!************************************************************************************!*\
  !*** ./src/app/layout/registered-posdevice/registered-posdevice-routing.module.ts ***!
  \************************************************************************************/
/*! exports provided: RegisteredPosdeviceRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegisteredPosdeviceRoutingModule", function() { return RegisteredPosdeviceRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _registered_posdevice_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./registered-posdevice.component */ "./src/app/layout/registered-posdevice/registered-posdevice.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var routes = [{
        path: '',
        component: _registered_posdevice_component__WEBPACK_IMPORTED_MODULE_2__["RegisteredPosdeviceComponent"]
    }];
var RegisteredPosdeviceRoutingModule = /** @class */ (function () {
    function RegisteredPosdeviceRoutingModule() {
    }
    RegisteredPosdeviceRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], RegisteredPosdeviceRoutingModule);
    return RegisteredPosdeviceRoutingModule;
}());



/***/ }),

/***/ "./src/app/layout/registered-posdevice/registered-posdevice.component.html":
/*!*********************************************************************************!*\
  !*** ./src/app/layout/registered-posdevice/registered-posdevice.component.html ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"login-page\" [@routerTransition]>\n    <div class=\"row justify-content-md-center\">\n        <div class=\"col-md-4 crytal-black\">\n            <img class=\"user-avatar\" src=\"assets/images/BA-logo.png\" width=\"70px\" />\n            <h1 class=\"card-title\">Register Devices</h1>\n            <form method=\"post\" role=\"form\" [formGroup]=\"form\" (submit)=\"onRegisterSubmit()\">\n                <div class=\"form-content\">\n                    <div class=\"form-group\">\n                        <div [ngClass]=\"{'has-error': (form.controls.imei.errors && form.controls.imei.dirty), 'has-success': !form.controls.imei.errors && imeiValid}\">\n                            <input type=\"text\" class=\"form-control input-underline input-lg\" placeholder=\"IMEI : 358472042445412\"\n                                name=\"imei\" formControlName=\"imei\" minlength=\"15\" maxlength=\"15\">\n                            <ul class=\"help-block text-white\">\n                                <li *ngIf=\"form.controls.imei.errors?.required && form.controls.imei.dirty && form.controls.imei.touched\">This\n                                    field is required</li>\n                                <li *ngIf=\"form.controls.imei.errors?.minlength && form.controls.imei.dirty || form.controls.imei.errors?.maxlength && form.controls.imei.dirty\">\n                                    Only 15 Digits Allowed</li>\n                                <li *ngIf=\"form.controls.imei.errors?.validateIMEI && form.controls.imei.dirty\">IMEI\n                                    must not have any special characters </li>\n                            </ul>\n                        </div>\n                    </div>\n\n                    <div class=\"form-group\">\n                        <div [ngClass]=\"{'has-error': (form.controls.sim.errors && form.controls.sim.dirty) || (!simValid && form.controls.sim.dirty), 'has-success': !form.controls.sim.errors && simValid}\">\n                            <input type=\"text\" class=\"form-control input-underline input-lg\" placeholder=\"SIM : 03001234567\"\n                                name=\"sim\" formControlName=\"sim\" minlength=\"11\" maxlength=\"11\">\n                            <ul class=\"help-block text-white\">\n                                <li *ngIf=\"form.controls.sim.errors?.required && form.controls.sim.dirty && form.controls.sim.touched\">This\n                                    field is required</li>\n                                <li *ngIf=\"form.controls.sim.errors?.minlength && form.controls.sim.dirty || form.controls.sim.errors?.maxlength && form.controls.sim.dirty\">\n                                    Only 11 Digits Allowed</li>\n                                <li *ngIf=\"form.controls.sim.errors?.validateSIM && form.controls.sim.dirty\">SIM\n                                    must not have any special characters </li>\n                            </ul>\n                        </div>\n                    </div>\n\n                    <div class=\"form-group\">\n                        <div [ngClass]=\"{'has-error': (form.controls.pan.errors && form.controls.pan.dirty) || (!simValid && form.controls.pan.dirty), 'has-success': !form.controls.pan.errors && panValid}\">\n                            <input type=\"text\" class=\"form-control input-underline input-lg\" placeholder=\"PAN: 012345678911123\"\n                                name=\"pan\" formControlName=\"pan\" minlength=\"15\" maxlength=\"15\">\n                            <ul class=\"help-block text-white\">\n                                <li *ngIf=\"form.controls.pan.errors?.required && form.controls.pan.dirty && form.controls.pan.touched\">This\n                                    field is required</li>\n                                <li *ngIf=\"form.controls.pan.errors?.minlength && form.controls.pan.dirty || form.controls.pan.errors?.maxlength && form.controls.pan.dirty\">\n                                    Only 15 Digits Allowed</li>\n                                <li *ngIf=\"form.controls.pan.errors?.validatePAN && form.controls.pan.dirty\">PAN\n                                    must not have any special characters </li>\n                            </ul>\n                        </div>\n                    </div>\n                    <!-- <div class=\"form-group\">\n                            <input type=\"text\" class=\"form-control input-underline input-lg\" placeholder=\"PAN(04) : 012345678911123\"\n                                name=\"pan1\" formControlName=\"pan1\" minlength=\"15\" maxlength=\"15\">\n                    </div> -->\n                    <div class=\"form-group\">\n                        <div [ngClass]=\"{'has-error': (form.controls.tid.errors && form.controls.tid.dirty) || (!tidValid && form.controls.tid.dirty), 'has-success': !form.controls.tid.errors && tidValid}\">\n                            <input type=\"text\" class=\"form-control input-underline input-lg\" placeholder=\"TID : 0123456789111234\"\n                                name=\"tid\" formControlName=\"tid\" minlength=\"16\" maxlength=\"16\">\n                            <ul class=\"help-block text-white\">\n                                <li *ngIf=\"form.controls.tid.errors?.required && form.controls.tid.dirty && form.controls.tid.touched\">This\n                                    field is required</li>\n                                <li *ngIf=\"form.controls.tid.errors?.minlength && form.controls.tid.dirty || form.controls.tid.errors?.maxlength && form.controls.tid.dirty\">\n                                    Only 16 Digits Allowed</li>\n                                <li *ngIf=\"form.controls.tid.errors?.validateTID && form.controls.tid.dirty\">TID\n                                    must not have any special characters </li>\n                            </ul>\n                        </div>\n                    </div>\n\n                    <div class=\"form-group\">\n                            <div [ngClass]=\"{'has-error': (form.controls.sid.errors && form.controls.sid.dirty) || (!sidValid && form.controls.sid.dirty), 'has-success': !form.controls.sid.errors && sidValid}\">\n                                <input type=\"text\" class=\"form-control input-underline input-lg\" placeholder=\"Store ID : 1234567\"\n                                    name=\"sid\" formControlName=\"sid\" minlength=\"7\" maxlength=\"7\">\n                                <ul class=\"help-block text-white\">\n                                    <li *ngIf=\"form.controls.sid.errors?.required && form.controls.sid.dirty && form.controls.sid.touched\">This\n                                        field is required</li>\n                                    <li *ngIf=\"form.controls.sid.errors?.minlength && form.controls.sid.dirty || form.controls.sid.errors?.maxlength && form.controls.sid.dirty\">\n                                        Only 7 Digits Allowed</li>\n                                    <li *ngIf=\"form.controls.sid.errors?.validateSID && form.controls.sid.dirty\">Store ID\n                                        must not have any special characters </li>\n                                </ul>\n                            </div>\n                        </div>\n                    \n                    <div class=\"form-group\">\n                        <div [ngClass]=\"{'has-error': (form.controls.merchant.errors && form.controls.merchant.dirty) || (!merchantValid && form.controls.merchant.dirty), 'has-success': !form.controls.merchant.errors && merchantValid}\">\n                            <input type=\"text\" class=\"form-control input-underline input-lg\" placeholder=\"Merchant Name: abcxyz\"\n                                name=\"merchant\" formControlName=\"merchant\" maxlength=\"25\">\n                            <ul class=\"help-block text-white\">\n                                <li *ngIf=\"form.controls.merchant.errors?.required && form.controls.merchant.dirty && form.controls.merchant.touched\">This\n                                    field is required</li>\n                                    <li *ngIf=\"form.controls.merchant.errors?.minlength && form.controls.merchant.dirty || form.controls.merchant.errors?.maxlength && form.controls.merchant.dirty\">\n                                            Not more than 25 characters allowed</li>\n                                <!-- <li *ngIf=\"form.controls.merchant.errors?.validateMerchant && form.controls.merchant.dirty\">Merchant\n                                    Name\n                                    must not have any special characters </li> -->\n                            </ul>\n                        </div>\n                    </div>\n\n                    <div class=\"form-group\">\n                        <select class=\"custom-select category\" name=\"mcategory\" formControlName=\"mcategory\">\n                            <option value=\"\" disabled selected>Merchant Category:</option>\n                            <option value=\"Supermarkets\"> Supermarkets</option>\n                            <option value=\"Electronic Sales\"> Electronic Sales</option>\n                            <option value=\"Fast Food Restaurants\"> Fast Food Restaurants</option>\n                            <option value=\"Eating places and Restaurants\"> Eating places and Restaurants</option>\n                            <option value=\"Household Appliance Stores\"> Household Appliance Stores</option>\n                            <option value=\"Book Stores\"> Book Stores</option>\n                            <option value=\"Sporting Goods Stores\"> Sporting Goods Stores</option>\n                        </select>\n                    </div>\n\n                </div>\n                <button class=\"btn rounded-btn\" type=\"reset\"> Clear </button>&nbsp;&nbsp;\n                <button class=\"btn rounded-btn\" type=\"submit\" [disabled]=\"!form.valid\"> Register </button>&nbsp;\n                <!-- <a class=\"btn rounded-btn\" [routerLink]=\"['/login']\"> Log in </a> -->\n            </form>\n        </div>\n    </div>\n</div>\n"

/***/ }),

/***/ "./src/app/layout/registered-posdevice/registered-posdevice.component.scss":
/*!*********************************************************************************!*\
  !*** ./src/app/layout/registered-posdevice/registered-posdevice.component.scss ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ":host {\n  display: block; }\n\n.login-page {\n  top: 0;\n  left: 0;\n  right: 0;\n  bottom: 0;\n  text-align: center;\n  color: #fff;\n  padding: 1em; }\n\n.login-page .crytal-black {\n    background: rgba(0, 0, 0, 0.658824);\n    padding: 25px; }\n\n.login-page .crytal-black .bottom {\n      padding-top: 8px;\n      font-size: 14px; }\n\n.login-page .crytal-black .bottom p {\n        margin-bottom: 0rem; }\n\n.login-page .crytal-black .bottom a {\n        color: #ced4da;\n        text-decoration: none; }\n\n.login-page .crytal-black .bottom a:hover {\n          color: #bc2418; }\n\n.login-page .crytal-black .forgot-pass {\n      float: right;\n      margin-top: -55px;\n      font-size: 13px; }\n\n.login-page .crytal-black .forgot-pass a {\n        color: #ced4da;\n        text-decoration: none; }\n\n.login-page .crytal-black .forgot-pass a:hover {\n          color: #bc2418; }\n\n.login-page .col-lg-4 {\n    padding: 0; }\n\n.login-page .input-lg {\n    height: 46px;\n    padding: 10px 16px;\n    font-size: 18px;\n    line-height: 1.3333333;\n    border-radius: 0; }\n\n.login-page .input-underline {\n    background: 0 0;\n    border: none;\n    box-shadow: none;\n    border-bottom: 2px solid rgba(255, 255, 255, 0.5);\n    color: #fff;\n    border-radius: 0; }\n\n.login-page .input-underline:focus {\n    border-bottom: 2px solid #fff;\n    box-shadow: none; }\n\n.login-page .rounded-btn {\n    border-radius: 50px;\n    color: rgba(255, 255, 255, 0.8);\n    background: #222;\n    border: 2px solid rgba(255, 255, 255, 0.8);\n    font-size: 18px;\n    line-height: 40px;\n    padding: 0 25px; }\n\n.login-page .rounded-btn:hover,\n  .login-page .rounded-btn:focus,\n  .login-page .rounded-btn:active,\n  .login-page .rounded-btn:visited {\n    color: white;\n    border: 2px solid white;\n    outline: none; }\n\n.login-page h1 {\n    font-weight: 300;\n    margin-top: 20px;\n    margin-bottom: 10px;\n    font-size: 36px; }\n\n.login-page h1 small {\n      color: rgba(255, 255, 255, 0.7); }\n\n.login-page .form-group {\n    padding: 8px 0; }\n\n.login-page .form-group input::-webkit-input-placeholder {\n      color: rgba(255, 255, 255, 0.6) !important; }\n\n.login-page .form-group input:-moz-placeholder {\n      /* Firefox 18- */\n      color: rgba(255, 255, 255, 0.6) !important; }\n\n.login-page .form-group input::-moz-placeholder {\n      /* Firefox 19+ */\n      color: rgba(255, 255, 255, 0.6) !important; }\n\n.login-page .form-group input:-ms-input-placeholder {\n      color: rgba(255, 255, 255, 0.6) !important; }\n\n.login-page .form-content {\n    padding: 40px 0; }\n\n.login-page .user-avatar {\n    -webkit-border-radius: 50%; }\n\n@media (min-width: 768px) and (max-width: 1200px) {\n    .login-page .col-md-4 {\n      flex: 0 0 42.333333%;\n      max-width: 42.333333%; } }\n\n.input-underline.ng-invalid.ng-touched, select.ng-invalid.ng-touched {\n  border-bottom: 2px solid red; }\n\n.help-block {\n  font-size: 14px;\n  float: left;\n  text-align: left; }\n\n.category {\n  border: none;\n  box-shadow: none;\n  background-color: #575757;\n  border-bottom: 2px solid rgba(255, 255, 255, 0.5);\n  color: #fff;\n  border-radius: 0; }\n"

/***/ }),

/***/ "./src/app/layout/registered-posdevice/registered-posdevice.component.ts":
/*!*******************************************************************************!*\
  !*** ./src/app/layout/registered-posdevice/registered-posdevice.component.ts ***!
  \*******************************************************************************/
/*! exports provided: RegisteredPosdeviceComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegisteredPosdeviceComponent", function() { return RegisteredPosdeviceComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _router_animations__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../router.animations */ "./src/app/router.animations.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _shared_services_bank_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../shared/services/bank.service */ "./src/app/shared/services/bank.service.ts");
/* harmony import */ var angular2_flash_messages__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! angular2-flash-messages */ "./node_modules/angular2-flash-messages/module/index.js");
/* harmony import */ var angular2_flash_messages__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(angular2_flash_messages__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var RegisteredPosdeviceComponent = /** @class */ (function () {
    function RegisteredPosdeviceComponent(formBuilder, flashMessages, service, router) {
        this.formBuilder = formBuilder;
        this.flashMessages = flashMessages;
        this.service = service;
        this.router = router;
        this.createForm();
    }
    RegisteredPosdeviceComponent.prototype.ngOnInit = function () {
    };
    RegisteredPosdeviceComponent.prototype.createForm = function () {
        this.form = this.formBuilder.group({
            // IMEI Input
            imei: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required,
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].minLength(15),
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].maxLength(15),
                    this.validateIMEI // Custom validation
                ])],
            // SIM Input
            sim: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required,
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].minLength(11),
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].maxLength(11),
                    this.validateSIM // Custom validation
                ])],
            // PAN Input
            pan: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required,
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].minLength(15),
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].maxLength(15),
                    this.validatePAN
                ])],
            // Validators.required, // Field is required
            // Validators.minLength(15), // Minimum length is 6 characters
            // Validators.maxLength(15), // Maximum length is 6 characters
            // this.validatePAN // Custom validation
            // pan1: [''],
            // TID Input
            tid: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required,
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].minLength(16),
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].maxLength(16),
                    this.validateTID // Custom validation
                ])],
            sid: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required,
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].minLength(7),
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].maxLength(7),
                    this.validateSID // Custom validation
                ])],
            // Merchant Name Input
            merchant: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required,
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].minLength(3),
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].maxLength(25)
                    // this.validateMerchant // Custom validation
                ])],
            mcategory: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required,
                ])]
        });
    };
    // Function to validate IMEI no is proper format
    RegisteredPosdeviceComponent.prototype.validateIMEI = function (controls) {
        var regExp = new RegExp(/^[0-9]{15,15}|[,]$/);
        if (regExp.test(controls.value)) {
            return null;
        }
        else {
            return { 'validateIMEI': true };
        }
    };
    // Function to validate SIM no is proper format
    RegisteredPosdeviceComponent.prototype.validateSIM = function (controls) {
        var regExp = new RegExp(/^[0-9]{11,11}|[,]$/);
        if (regExp.test(controls.value)) {
            return null;
        }
        else {
            return { 'validateSIM': true };
        }
    };
    // Function to validate PAN no is proper format
    RegisteredPosdeviceComponent.prototype.validatePAN = function (controls) {
        var regExp = new RegExp(/^[0-9]{6,6}|[,]$/);
        if (regExp.test(controls.value)) {
            return null;
        }
        else {
            return { 'validatePAN': true };
        }
    };
    RegisteredPosdeviceComponent.prototype.validateTID = function (controls) {
        var regExp = new RegExp(/^[0-9]{16,16}|[,]$/);
        if (regExp.test(controls.value)) {
            return null;
        }
        else {
            return { 'validateTID': true };
        }
    };
    RegisteredPosdeviceComponent.prototype.validateSID = function (controls) {
        var regExp = new RegExp(/^[0-9]{7,7}|[,]$/);
        if (regExp.test(controls.value)) {
            return null;
        }
        else {
            return { 'validateSID': true };
        }
    };
    // validateMerchant(controls) {
    //   const regExp = new RegExp(/^[a-zA-Z][a-zA-Z ]+$/);
    //   if (regExp.test(controls.value)) {
    //     return null;
    //   } else {
    //     return { 'validateMerchant': true };
    //   }
    // }
    RegisteredPosdeviceComponent.prototype.onRegisterSubmit = function () {
        var _this = this;
        var deviceobj = {
            imei: this.form.get('imei').value,
            sim: this.form.get('sim').value,
            pan: this.form.get('pan').value,
            // pan1: this.form.get('pan1').value,
            mn: this.form.get('merchant').value,
            tid: this.form.get('tid').value,
            storeid: this.form.get('sid').value,
            mc: this.form.get('mcategory').value
        };
        console.log(deviceobj);
        // const token = this.service.getToken();
        this.service.registerDevice(deviceobj).subscribe(function (data) {
            if (data.code == "successful") {
                alert("Device Successfully Registered");
                _this.flashMessages.show('Device Registered!', { cssClass: 'alert-success', timeout: 2000 });
                _this.form.reset();
                console.log(data);
            }
            else {
                alert(data.message);
                _this.flashMessages.show(data.message, { cssClass: 'alert-danger', timeout: 2000 });
            }
        });
    };
    RegisteredPosdeviceComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-registered-posdevice',
            template: __webpack_require__(/*! ./registered-posdevice.component.html */ "./src/app/layout/registered-posdevice/registered-posdevice.component.html"),
            styles: [__webpack_require__(/*! ./registered-posdevice.component.scss */ "./src/app/layout/registered-posdevice/registered-posdevice.component.scss")],
            animations: [Object(_router_animations__WEBPACK_IMPORTED_MODULE_1__["routerTransition"])()]
        }),
        __metadata("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"],
            angular2_flash_messages__WEBPACK_IMPORTED_MODULE_4__["FlashMessagesService"],
            _shared_services_bank_service__WEBPACK_IMPORTED_MODULE_3__["BankService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"]])
    ], RegisteredPosdeviceComponent);
    return RegisteredPosdeviceComponent;
}());



/***/ }),

/***/ "./src/app/layout/registered-posdevice/registered-posdevice.module.ts":
/*!****************************************************************************!*\
  !*** ./src/app/layout/registered-posdevice/registered-posdevice.module.ts ***!
  \****************************************************************************/
/*! exports provided: RegisteredPosdeviceModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegisteredPosdeviceModule", function() { return RegisteredPosdeviceModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/index.js");
/* harmony import */ var _registered_posdevice_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./registered-posdevice-routing.module */ "./src/app/layout/registered-posdevice/registered-posdevice-routing.module.ts");
/* harmony import */ var _registered_posdevice_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./registered-posdevice.component */ "./src/app/layout/registered-posdevice/registered-posdevice.component.ts");
/* harmony import */ var _shared__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./../../shared */ "./src/app/shared/index.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







var RegisteredPosdeviceModule = /** @class */ (function () {
    function RegisteredPosdeviceModule() {
    }
    RegisteredPosdeviceModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _registered_posdevice_routing_module__WEBPACK_IMPORTED_MODULE_3__["RegisteredPosdeviceRoutingModule"],
                _shared__WEBPACK_IMPORTED_MODULE_5__["PageHeaderModule"],
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_2__["NgbModule"].forRoot(),
                _angular_forms__WEBPACK_IMPORTED_MODULE_6__["ReactiveFormsModule"]
            ],
            declarations: [_registered_posdevice_component__WEBPACK_IMPORTED_MODULE_4__["RegisteredPosdeviceComponent"]]
        })
    ], RegisteredPosdeviceModule);
    return RegisteredPosdeviceModule;
}());



/***/ })

}]);
//# sourceMappingURL=registered-posdevice-registered-posdevice-module.js.map