(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["realtime-monitoring-realtime-monitoring-module"],{

/***/ "./src/app/layout/realtime-monitoring/realtime-monitoring-routing.module.ts":
/*!**********************************************************************************!*\
  !*** ./src/app/layout/realtime-monitoring/realtime-monitoring-routing.module.ts ***!
  \**********************************************************************************/
/*! exports provided: RealtimeMonitoringRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RealtimeMonitoringRoutingModule", function() { return RealtimeMonitoringRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _realtime_monitoring_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./realtime-monitoring.component */ "./src/app/layout/realtime-monitoring/realtime-monitoring.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var routes = [{
        path: '',
        component: _realtime_monitoring_component__WEBPACK_IMPORTED_MODULE_2__["RealtimeMonitoringComponent"]
    }];
var RealtimeMonitoringRoutingModule = /** @class */ (function () {
    function RealtimeMonitoringRoutingModule() {
    }
    RealtimeMonitoringRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], RealtimeMonitoringRoutingModule);
    return RealtimeMonitoringRoutingModule;
}());



/***/ }),

/***/ "./src/app/layout/realtime-monitoring/realtime-monitoring.component.html":
/*!*******************************************************************************!*\
  !*** ./src/app/layout/realtime-monitoring/realtime-monitoring.component.html ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"form-group col-md-4 filterdiv\">\n  <input class=\"form-control\" [(ngModel)]=\"searchText\" placeholder=\"Filter By IMEI\">\n</div>\n<div class=\"table-responsive\">\n<table class=\"table table-hover\">\n    <thead>\n      <tr>\n        <th>#</th>\n        <th scope=\"col\">IMEI</th>\n        <th scope=\"col\">TID</th>\n        <th scope=\"col\">Merchant Name</th>\n        <th scope=\"col\">Last Transaction</th>\n        <th scope=\"col\">Last Ping</th>\n      </tr> \n    </thead>\n    <tbody>\n        <!-- *ngFor=\"let list of report | filter: searchText  | paginate: { itemsPerPage: 10, currentPage: p } let i= index\" -->\n      <tr *ngFor=\"let item of realdata  | filter: searchText | orderBy:reverse | paginate: { itemsPerPage: 25, currentPage: p }; let i=index\">\n          <td>{{ i+1 }}</td>          \n          <td>{{ item.imei }}</td>\n          <td>{{ item.tid }}</td>\n          <td>{{ item.mn }}</td>\n          <td><strong> {{ item.lt }}</strong></td>\n          <td><strong> {{ item.lp }}</strong></td>\n        <!-- <td>{{  }}</td>          \n        <td>{{  }}</td>\n        <td>{{  }}</td>\n        <td>{{  }}</td>\n        <td>{{  }}</td>\n        <td>{{  }}</td> -->\n      </tr>\n    </tbody>\n  </table>\n  </div>\n  <pagination-controls *ngIf=\"realdata\" class=\"my-pagination\" (pageChange)=\"p = $event\"></pagination-controls>\n"

/***/ }),

/***/ "./src/app/layout/realtime-monitoring/realtime-monitoring.component.scss":
/*!*******************************************************************************!*\
  !*** ./src/app/layout/realtime-monitoring/realtime-monitoring.component.scss ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".table {\n  background-color: #000000de;\n  color: white;\n  font-size: 14px; }\n\n.filterdiv {\n  margin-right: -15px;\n  float: right; }\n\n.filterdiv input {\n    border: 0;\n    border-bottom: 2px solid #e41a06;\n    background-color: #212121;\n    color: white; }\n\n.my-pagination /deep/ .ngx-pagination {\n  margin-top: 20px;\n  background: #575757 !important; }\n"

/***/ }),

/***/ "./src/app/layout/realtime-monitoring/realtime-monitoring.component.ts":
/*!*****************************************************************************!*\
  !*** ./src/app/layout/realtime-monitoring/realtime-monitoring.component.ts ***!
  \*****************************************************************************/
/*! exports provided: RealtimeMonitoringComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RealtimeMonitoringComponent", function() { return RealtimeMonitoringComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _shared_services_bank_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../shared/services/bank.service */ "./src/app/shared/services/bank.service.ts");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var RealtimeMonitoringComponent = /** @class */ (function () {
    function RealtimeMonitoringComponent(service) {
        this.service = service;
    }
    RealtimeMonitoringComponent.prototype.ngOnInit = function () {
        var _this = this;
        // const source = timer(0, 5000);
        // this.subscribe = source.subscribe( t=> {
        //   const token = this.service.getToken();
        //   this.products$ = this.service.RealTimeMonitoring(token);
        //   console.log(this.products$);
        // });
        var source = Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["timer"])(0, 5000);
        this.subscribe = source.subscribe(function (t) {
            _this.RealTimeData();
        });
    };
    RealtimeMonitoringComponent.prototype.ngOnDestroy = function () {
        this.subscribe.unsubscribe();
    };
    RealtimeMonitoringComponent.prototype.RealTimeData = function () {
        var _this = this;
        // const token = this.service.getToken();
        this.service.RealTimeMonitoring().subscribe(function (data) {
            _this.realdata = data;
            console.log(_this.realdata);
        }, function (err) {
            if (err.status == 0 || err.status == 401) {
                _this.service.RemoveToken();
                console.log('Session Expired');
            }
        });
    };
    RealtimeMonitoringComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-realtime-monitoring',
            template: __webpack_require__(/*! ./realtime-monitoring.component.html */ "./src/app/layout/realtime-monitoring/realtime-monitoring.component.html"),
            styles: [__webpack_require__(/*! ./realtime-monitoring.component.scss */ "./src/app/layout/realtime-monitoring/realtime-monitoring.component.scss")]
        }),
        __metadata("design:paramtypes", [_shared_services_bank_service__WEBPACK_IMPORTED_MODULE_1__["BankService"]])
    ], RealtimeMonitoringComponent);
    return RealtimeMonitoringComponent;
}());



/***/ }),

/***/ "./src/app/layout/realtime-monitoring/realtime-monitoring.module.ts":
/*!**************************************************************************!*\
  !*** ./src/app/layout/realtime-monitoring/realtime-monitoring.module.ts ***!
  \**************************************************************************/
/*! exports provided: RealtimeMonitoringModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RealtimeMonitoringModule", function() { return RealtimeMonitoringModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _realtime_monitoring_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./realtime-monitoring-routing.module */ "./src/app/layout/realtime-monitoring/realtime-monitoring-routing.module.ts");
/* harmony import */ var _realtime_monitoring_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./realtime-monitoring.component */ "./src/app/layout/realtime-monitoring/realtime-monitoring.component.ts");
/* harmony import */ var _shared__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../shared */ "./src/app/shared/index.ts");
/* harmony import */ var _my_pipe_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../my-pipe.module */ "./src/app/layout/my-pipe.module.ts");
/* harmony import */ var ngx_order_pipe__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ngx-order-pipe */ "./node_modules/ngx-order-pipe/ngx-order-pipe.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};








var RealtimeMonitoringModule = /** @class */ (function () {
    function RealtimeMonitoringModule() {
    }
    RealtimeMonitoringModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _realtime_monitoring_routing_module__WEBPACK_IMPORTED_MODULE_3__["RealtimeMonitoringRoutingModule"],
                _shared__WEBPACK_IMPORTED_MODULE_5__["PageHeaderModule"],
                _my_pipe_module__WEBPACK_IMPORTED_MODULE_6__["MyPipeModule"],
                ngx_order_pipe__WEBPACK_IMPORTED_MODULE_7__["OrderModule"]
            ],
            declarations: [_realtime_monitoring_component__WEBPACK_IMPORTED_MODULE_4__["RealtimeMonitoringComponent"]]
        })
    ], RealtimeMonitoringModule);
    return RealtimeMonitoringModule;
}());



/***/ })

}]);
//# sourceMappingURL=realtime-monitoring-realtime-monitoring-module.js.map